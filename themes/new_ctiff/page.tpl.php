<?php
  $rootheme = check_url($front_page) .'sites/all/themes/new_ctiff';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta content='ie=edge' http-equiv='x-ua-compatible'>
    <meta content='' name='ctiff, coral triangle'>
    <meta content='width=device-width, initial-scale=1' name='viewport'>
    <link href='apple-touch-icon.png' rel='apple-touch-icon'>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <style>
      header.page-header .navbar-mega-menu .navbar-nav>li>a {
        font-size: 13px;
        padding: 12px 7px;
      }
    </style>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if lt IE 7]>
      <?php print phptemplate_get_ie_styles(); ?>
    <![endif]-->
    <script>
      $(window).load(function(){
        ls = function(){
        $('.pubdlcnt_file').each(function(){
        var aa = $(this).find('a').text(); 
        if(aa == "") $(this).hide();
        });}
        ls()
      });
    </script>
  </head>
  <body class='page page-index' >
    <button class="btn btn-info" onclick="goToTop();" id="top-btn"><i class="fa fa-angle-up"></i></button>
    <!-- end of top btn -->
    <?php print $navbar_mobile_left; ?>
    <div class='mobile-search'>
      <div class='search'>
        <form class="search-form" id="search-form" method="post" accept-charset="UTF-8" action="<?php echo base_path() . 'search/'; ?>">
          <div class='form-group'>
            <input name='keys' id='edit-keys' placeholder='SEARCH' type='text'>
          </div>
        </form>
      </div>
    </div>
    <div class='mainpage'>
      <header class='page-header'>
        <div class='header'>
          <div class='top-header'>
            <div class='container clearfix'>
              <a class='official-login' href='<?php echo check_url($front_page) . 'user' ?>'>
                <i class='fa fa-user'></i>
                <?php if(user_is_logged_in()): ?>
                  My Account
                <?php else: ?>
                  Official's Login
                <?php endif; ?>
              </a>
              <?php print $top_header; ?>
            </div>
          </div>
          <div class='bottom-header'>
            <div class='container'>
              <div class='row'>
                <div class='col-md-8 col-sm-12'>
                  <div class='logo'>
                    <a href="<?php echo check_url($front_page) ?>" title="<?php echo  $site_title  ?>">
                      <img class="header-logo" alt='' src='<?php echo $rootheme; ?>/images/header/cti-logo.png'>
                    </a>
                  </div>
                  <div class='country-flag'>
                    <?php print $country_flag; ?>
                  </div>
                </div>
                <div class='col-md-4 col-sm-12 hidden-sm hidden-xs'>
                  <div class='search'>
                    <form class="search-form" id="search-form" method="post" accept-charset="UTF-8" action="<?php echo base_path() . 'search/'; ?>">
                      <div class='form-group'>
                        <input name='keys' id='edit-keys' placeholder='SEARCH' type='text'>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <nav class='navbar navbar-default navbar-mobile hidden-lg hidden-md'>
          <div class='container'>
            <div class='row'>
              <div class='col-md-6 col-sm-6 col-xs-6'>
                <button class='toggleMenu' type='button'>
                  <i class='fa fa-bars'></i>
                  Menu
                </button>
              </div>
              <div class='col-md-6 col-sm-6 col-xs-6 border-menu'>
                <button class='toggelSearch' type='button'>
                  <i class='fa fa-search'></i>
                  Search
                </button>
              </div>
            </div>
          </div>
        </nav>
        <?php print $navbar_mega_menu; ?>
      </header>
      <?php print $main_slideshow; ?>
      <main class='page-content'>
        <?php if($messages): ?>
          <div class="container"><?php print $messages; ?></div>
        <?php endif; ?>
        <?php if(!empty($content) && !empty($aside_right_content)): ?>
          <div class="clearfix block-twin">
            <div class="container">
              <div class="row">
                <div class="col-md-9 col-sm-8 col-xs-12 main-left"><?php print $content; ?></div>
                <div class="col-md-3 col-sm-4 col-xs-12 aside-right"><?php print $aside_right_content; ?></div>
              </div>
            </div>
          </div>
        <?php elseif(!empty($content) && empty($aside_right_content)): ?>
          <?php print $content; ?>
        <?php endif; ?>
        <?php if(!empty($block_left) && !empty($block_right)): ?>
          <div class="clearfix block-twin">
            <div class="container">
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12"><?php print $block_left; ?></div>
                <div class="col-md-6 col-sm-6 col-xs-12"><?php print $block_right; ?></div>
              </div>
            </div>
          </div>
        <?php endif; ?>
        <?php if(!empty($main_left) && !empty($aside_right)): ?>
          <div class="clearfix block-twin">
            <div class="container">
              <div class="row">
                <div class="col-md-9 col-sm-8 col-xs-12 main-left"><?php print $main_left; ?></div>
                <div class="col-md-3 col-sm-4 col-xs-12 aside-right"><?php print $aside_right; ?></div>
              </div>
            </div>
          </div>
        <?php endif; ?>
        <?php if(!empty($main_right) && !empty($aside_left)): ?>
          <div class="clearfix block-twin">
            <div class="container">
              <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12 aside-left"><?php print $aside_left; ?></div>
                <div class="col-md-9 col-sm-8 col-xs-12 main-right"><?php print $main_right; ?></div>
              </div>
            </div>
          </div>
        <?php endif; ?>
        <?php if(!empty($block_page_cs)): ?>
          <div class="block_page_cs">
            <?php print $block_page_cs; ?>
          </div>
        <?php endif; ?>
        <?php if(!empty($block_left_bottom) && !empty($block_right_bottom)): ?>
          <div class="clearfix block-twin">
            <div class="container">
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12"><?php print $block_left_bottom; ?></div>
                <div class="col-md-6 col-sm-6 col-xs-12"><?php print $block_right_bottom; ?></div>
              </div>
            </div>
          </div>
        <?php endif; ?>
      </main>
    </div>
    <footer class='page-footer'>
      <div class='footer-meta'>
        <div class='container'>
          <div class='row'>
            <div class='col-md-9 col-sm-7 col-xs-12 clearfix'>
              <div class='footer-logo hidden-sm hidden-xs'>
                <a href="<?php echo check_url($front_page) ?>" title="<?php echo  $site_title  ?>">
                  <img alt='' src='<?php echo $rootheme; ?>/images/footer/cti-logo.png'>
                </a>
              </div>
              <div class='footer-menu clearfix'>
                <?php print $footer_menu; ?>
              </div>
            </div>
            <div class='col-md-3 col-sm-5 col-xs-12'>
              <div class='social-link-footer'>
                <?php print $footer_social_link; ?>
              </div>
              <?php print $footer_subscriber; ?>
            </div>
          </div>
        </div>
      </div>
      <div class='copyrights'>
        <div class='container'>
          <p class='text-center'>
            Copyrights &copy; <?php echo date('Y'); ?> Coral Triangle Initiative On Coral Reefs, Fisheries and Food Security
          </p>
        </div>
      </div>
    </footer>
    <?php print $closure; ?>
  </body>
  <script type="text/javascript">
    $(document).ready(function() {

      

      function checkImg(img) {
        if (img.naturalHeight <= 1 && img.naturalWidth <= 1) {
          // undersize image here
          img.src = "<?php print $rootheme; ?>/images/section/default-picture.jpg";
        }
      }
      replaceImage = function(){
        $("img").each(function() {
          // if image already loaded, we can check it's height now
          if (this.complete) {
            checkImg(this);
          } else {
            // if not loaded yet, then set load and error handlers
            $(this).load(function() {
                checkImg(this);
            }).error(function() {
                // img did not load correctly
                // set new .src here
                this.src = "<?php print $rootheme; ?>/images/section/default-picture.jpg";
            });
          }
        });
      };
      replaceImage();
      
      $('.pager li a').click(function(){
        setTimeout(function(){ replaceImage(); }, 8000);
        
      });
    });
  </script>
</html>
