  <script>
      /* Javascript for popUp login Box*/
      var popupStatus = 0;      
      //loading popup with jQuery magic!
      function loadPopup(){
        //loads popup only if it is disabled
        if(popupStatus==0){
          $("#backgroundPopup").css({
                  "opacity": "0.7"
          });
          $("#backgroundPopup").fadeIn("slow");
          $("#resourceByCategory").fadeIn("slow");
          popupStatus = 1;
          //var src =  "<?php print base_path().path_to_theme();?>/images/minus.png";
          //$("#seeAllSubjects").attr("src", src);
          
          //Center the pop up
          centerPopup();
          
          ie_hack_disable();
          if (document.all) {
                var windowWidth = document.documentElement.clientWidth;              
                var windowHeight = document.documentElement.clientHeight;
                $("#backgroundPopup").height(6000);
                $("#backgroundPopup").width(windowWidth);
                $(document).scroll()
                /*$("#backgroundPopup").css({
                    "height": windowHeight,
                    "width" : windowWidth,
                });*/
          }
        }
      }
      
    function getInternetExplorerVersion()// Returns the version of Windows Internet Explorer or a -1    // (indicating the use of another browser)      
    {
        var rv = -1; // Return value assumes failure.
        if (navigator.appName == 'Microsoft Internet Explorer')
        {
            var ua = navigator.userAgent;
            var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
            if (re.exec(ua) != null)
            rv = parseFloat( RegExp.$1 );
        }
        return rv;
   }
      
      
      //disabling popup with jQuery magic!
      function disablePopup(){
        //disables popup only if it is enabled
        if(popupStatus==1){
            $("#backgroundPopup").fadeOut("slow");
            $("#resourceByCategory").fadeOut("slow");
            popupStatus = 0;
            
            var src =  "<?php print base_path().path_to_theme();?>/css/images/plus.png";
            $("#seeAllSubjects").attr("src", src);
            //for ie
            ie_hack_enable();
        }
      }
      
      //centering popup
      function centerPopup(){        
        //request data for centering
        var windowWidth = document.documentElement.clientWidth;              
        var windowHeight = document.documentElement.clientHeight;
        var windowHeight = screen.height;
        var popupHeight = $("#resourceByCategory").height();
        var popupWidth = $("#resourceByCategory").width();
        
        //centering        
        $("#resourceByCategory").css({                
                "top": windowHeight/3-popupHeight/2,
                "left": windowWidth/2-popupWidth/2-20              
        });
        
        //only need force for IE6        
        $("#backgroundPopup").css({
                "height": windowHeight,
                "z-index" : 100
        });
      }
      
      //CONTROLLING EVENTS IN jQuery
      $(document).ready(function(){              
          //LOADING POPUP
          //Click the button event!
          $("#seeAllSubjects").click(function(){
                  //centering with css
                  //centerPopup();
                  //load popup
                  loadPopup();
                  return false;
          });
                                  
          //CLOSING POPUP
          //Click the x event!
          $("#popupLoginClose").click(function(){
                  disablePopup();
          });
          //Click out event!
          $("#backgroundPopup").click(function(){
                  disablePopup();
          });
          //Press Escape event!
          $(document).keypress(function(e){
                  if(e.keyCode==27 && popupStatus==1){
                          disablePopup();
                  }
          });      
      })
      

   
   
   function ie_hack_disable()
   {
            //checking for IE greater >=7 & disable some pictures and divs so that it does not come in the top of lightbox
            if(getInternetExplorerVersion()==7 || getInternetExplorerVersion()==6)
            {
                $("#bottom_rotor_container").hide();
                $("#homepage_right_top_b").hide();
                $("#homepage_extrem_top").hide();
            }
       
   }
    
    function ie_hack_enable()
   {
         //checking for IE greater >=7 & disable some pictures and divs so that it does not come in the top of lightbox
            if(getInternetExplorerVersion()==7 || getInternetExplorerVersion()==6)
            {
                $("#bottom_rotor_container").show();
                $("#homepage_right_top_b").show();
                $("#homepage_extrem_top").show();
            }
   }
   
   
  </script>
  
  <style>
    /* Stylesheet for popup login box. Be care to modify it*/
    #backgroundPopup {
        background:#000000 none repeat scroll 0%;
        border:1px solid #CECECE;
        display:none;
        height:100%;
        left:0pt;
        position:fixed;
        top:0pt;
        width:100%;
        z-index:100;
    }
    #resourceByCategory {
        background:#FFFFFF none repeat scroll;
        background-position:center;
        border:10px solid #CECECE;
        display:none;
        /*height:274px;*/
        padding:12px;
        position:fixed;
        width:80%;
        z-index:200;
        /*margin-top:-20% !important;
        margin-left:-27% !important;*/
    }
    #resourceByCategory h1 {
        background:transparent no-repeat scroll 0% 0%;
        color:#FBB815;
        font-weight:700;
        margin-bottom:20px;
        padding-bottom:2px;
        text-align:left;
            padding-left:45px;
            margin-left:-3px;
    }
    
    #popupLoginClose {
        color:#FBB815;
        display:block;
        font-weight:700;
        line-height:14px;
        position:absolute;
        right:6px;
        top:4px;
        cursor: pointer;
    }
    
    #user-login .form-text {
        width: 200px;
        border: 1px solid #c0c0c0;
    }
  
  </style>
  
  <div id="resourceByCategory">
    
    <a id="popupLoginClose"><i class="fa fa-close"></i></a>
        <?php //if (!empty($title)) : ?>
            <h3>ALL SUBJECTS<?php //print $title; ?></h3>
        <?php //endif; ?>
        <?php
            function get_documents_ids_having_resource2($ids){
                $sql =" SELECT tid FROM term_node WHERE tid in(".implode(',',$ids).") GROUP BY tid";
                $result_set = db_query($sql);
                while($row = db_fetch_array($result_set))
                {
                    $id[] = $row['tid'];
                }
                return $id;
            }
            //Results coming from view
            $viewRes_all = $view->result;
            foreach($viewRes_all as $record)
            {
                $ids[]=$record->tid;
            }
            //function for checking if there is resource in the database
            $ids_having_resource = get_documents_ids_having_resource2($ids);
            
            //Removing all the resources having no record in the database
            foreach($viewRes_all as $record)
            {
                //if(in_array($record->tid,$ids_having_resource))
                $viewRes[]=$record;
            }
            
            
            $total_rows = (int)(count($viewRes)/4);
            if(count($viewRes) % 4)
            {
                ++$total_rows;
            }
                     print' <table class="views-view-grid"><tbody>';
                    for($row = 1;$row<=$total_rows; $row++)
                        {
                            print '<tr>';
                            $kk=$row-1;
                            for($col=1;$col<=4;$col++)
                            {
                                if($col > 1)
                                    $kk +=$total_rows;
                               // print'<td>'.$kk.'</td>';
                                $values =  $viewRes[$kk];
                                $termsTid = $values->tid;
                                $termsName = ucwords($values->term_data_name);
                                $vocabulary = taxonomy_vocabulary_load($values->term_data_vid);
                                if($vocabulary->name=='Type'){
                                    $term_path='resource_by_type/'.$termsTid;
                                }
                                else if($vocabulary->name=='Subject'){
                                    $term_path='resource_by_subject/'.$termsTid;
                                }
                                else if($vocabulary->name=='Country'){
                                    $term_path='resource_by_country/'.$termsTid;
                                }
                                
                                
                                if($termsName)
                                {
                                    print $links = '<td><span id="'.$termsTid.'" class="vidTermClass term-' . $termsTid . '">' . l($termsName, $term_path, array('rel' => 'tag', 'title' => strip_tags($vocabulary->description))) .'</span></td>';
                                }
                                else
                                {
                                    print'<td>&nbsp;</td>';
                                }
                            }
                            print '</tr>';
                        }
                        print'</tbody></table>';
                ?>
       </div>
  
    <div id="backgroundPopup"></div>
    