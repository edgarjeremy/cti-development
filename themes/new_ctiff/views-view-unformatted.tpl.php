<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */

global $base_url;
global $base_path;

$breadcrumb = array();
$breadcrumb[] = l('Home', '');
$breadcrumb[] = l(drupal_get_title(), $base_url . base_path() . $view->display_handler->options['path']);

// Set Breadcrumbs
$breadcrumbs = drupal_set_breadcrumb($breadcrumb);
$title = $view->display_handler->options['title'];
?>
<div class="views-unformatted-style">
  <div class="entry-header">
    <div class="content-header">
      <div class="breadcrumb">
        <ol class="breadcrumb">
          <?php foreach($breadcrumbs as $breadcrumb): ?>
            <li><?php echo $breadcrumb; ?></li>
          <?php endforeach; ?>
        </ol>
      </div>
      <?php if (!empty($title)) : ?>
        <h1><?php print $title; ?></h1>
      <?php endif; ?>
    </div>
  </div>
  <div class="container">
    <?php foreach ($rows as $id => $row): ?>
      <div class="<?php print $classes[$id]; ?>">
        <?php print $row; ?>
      </div>
    <?php endforeach; ?>
  </div>
</div>
