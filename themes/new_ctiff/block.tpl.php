<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block block block-<?php print $block->module ?>">
    <?php
    //Change page title for Taxonomy Term page
    $get_url_data=explode('/',$_SERVER['REQUEST_URI']);
    
    //pr_disp($get_url_data);
    //pr_disp($block);
    
    $get_block_id=$block->bid;    
    $get_type=$get_url_data[1];
    $get_taxonomy_tid=$get_url_data[2];;
    
    $get_term_detials=taxonomy_get_term($get_taxonomy_tid);
    $vocabulary = taxonomy_vocabulary_load($get_term_detials->vid);
    
    if(($get_type=='resource_by_subject') && ($block->subject=='resource_by_subject')){
      $page_title='Resources by Subject - '.$get_term_detials->name;    
    }
    else if(($get_type=='resource_by_type') && ($block->subject=='resource_by_type')){
      $page_title='Resources by Type - '.$get_term_detials->name;  
    }
    else if($get_type=='resource_by_country' && ($block->subject=='resource_by_country')){
	    $page_title='Resources by Country - '.$get_term_detials->name;    
    }
    else {
      $page_title = $block->subject;
    }
    ?>
  
  <?php if (!empty($page_title)): ?>
    <h2><?php print $page_title; ?></h2>
  <?php endif;?>
  
  <div class="content"><?php print $block->content ?></div>
</div>
