<div class="node <?php print $classes; ?>" id="node-<?php print $node->nid; ?>">
   
  <div class="node-inner">
     <?php if (!$page): ?>
       <div class="">
        <h2 class="title"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
       </div>
     <?php endif; ?>
     <?php if ($page): ?>
       <div class="">
        <h1 class="my-header"><?php print $title; ?></h1>
       </div>
     <?php endif; ?>
     
     <div class="content">
       <div class="node_body_data">
        
        <?php
        if(!empty($node->field_version_pub[0]['value'])){
        ?>
        <div class="node_fields">
          <span class="node_label">Version note: </span>
          <?php            
            print $node->field_version_pub[0]['value'];
          ?>
        </div>   
        <?php
        }
        ?>
        
        <?php
        if(!empty($node->field_document_date[0]['value'])){
        ?>
        <div class="node_fields">
          <span class="node_label">Document Date: </span>
          <?php
          
           $date= strtotime($node->field_document_date[0]['value']);
           echo date('d F Y',$date);
            
          ?>
        </div>   
        <?php
        }
        ?>
        
        <?php
        if(!empty($node->field_institutional_author[0]['value'])){
        ?>
        <div class="node_fields">
          <span class="node_label">Author: </span>
          <?php            
            print $node->field_institutional_author[0]['value'];
          ?>
        </div>   
        <?php
        }
        ?>
        
        
        <?php
        if(!empty($node->field_video[0]['url'])){
        ?>
        <div class="node_fields">
          <span class="node_label">Video URL: </span>
          <?php            
            print $node->field_video[0]['url'];
          ?>
        </div>   
        <?php
        }
        ?>
        
        <?php
        if(!empty($node->field_audio[0]['url'])){
        ?>
        <div class="node_fields">
          <span class="node_label">Audio URL: </span>
          <?php            
            print $node->field_audio[0]['url'];
          ?>
        </div>   
        <?php
        }
        ?>        
        
        <?php
        if(!empty($node->field_executive_summary[0]['value'])){
        ?>
        <div class="node_fields">
          <span class="node_label">Executive Summary: </span>
          <?php            
            print $node->field_executive_summary[0]['value'];
          ?>
        </div>   
        <?php
        }
        ?>
        
        <div class="content_file_links">
          <span class="my-subtitle">Download/preview files:</span>
          <div class="container-fluid">
            <table class="table table-striped text-left">
              <tr>
                <th>#</th>
                <th>Documents</th>
              </tr>
            <?php
            
            $field_file_location_url = $node->field_file_location_url[0]['url'];
            $file_location_url = '<a href="'.$field_file_location_url.'"><b>Download file from its source</b></a>';
            $field_file_location_page_url = $node->field_file_location_page_url[0]['url'];
            $field_file_location_page_url_query = $node->field_file_location_page_url[0]['query'];
            if(!empty($field_file_location_page_url_query)){
              $field_file_location_page_url_data=$field_file_location_page_url.'?'.$field_file_location_page_url_query;
            }
            else{
              $field_file_location_page_url_data=$field_file_location_page_url;
            }
            $file_page_url = '<a href="'.$field_file_location_page_url_data.'" target="_blank" title="Go to the website hosting this document"><b>Go to the website hosting this document</b></a>';
            
            $node_ref=$node->field_executive_summary[0]['nid'];
            
            //Load the Node Reference data
            $get_ref_data=node_load($node_ref);
            $get_ref_data_files=$get_ref_data->files;
            $file_url = '';
            $file_url_executive = ''; 
            
            //Get all the file attachemnts
            if(!empty($get_ref_data_files)){
              $node_files = $get_ref_data_files;
              foreach($node_files as $file){
                $filedescription=$file->description;
                $filedate=date('F Y',$file->timestamp);
                $filepath=$file->filepath;
                
                $file_type=explode('.',$filepath);
                $filetype_disp=trim(strtolower($file_type[count($file_type)-1]));
                
                if($filetype_disp=='pdf'){
                  $img_disp='<img src="'.base_path().path_to_theme().'/images/doc_types/icon_'.$filetype_disp.'.gif" title="'.$title.'" alt="'.$title.'">';
                }
                else if($filetype_disp=='doc' || $filetype_disp=='docx'){
                  $change_type='doc';
                  $img_disp='<img src="'.base_path().path_to_theme().'/images/doc_types/icon_'.$change_type.'.gif" title="'.$title.'" alt="'.$title.'">';
                }
                else {
                  $change_type='generic';
                  $img_disp='<img src="'.base_path().path_to_theme().'/images/doc_types/icon_'.$change_type.'.gif" title="'.$title.'" alt="'.$title.'">';
                }
                
                /*$file_path_for_download_exe = html_entity_decode($file->filepath);
                $file_download_path_exe=base_path().drupal_get_path('module', 'pubdlcnt') .'/pubdlcnt.php?file='.base_path().$file_path_for_download_exe.'&nid='.$node->nid;
                
                $file_url_executive .= '<li>'.$img_disp.' <a href="'.$file_download_path_exe.'" title="'.$title.'"><b>Download Executive summary</b></a></li>';*/
                
                $file_path_for_download_exe = $file->filepath;
                if(!(stripos($file_path_for_download_exe,'&'))){
                  $file_download_path_exe = base_path().drupal_get_path('module', 'pubdlcnt') .'/pubdlcnt.php?file='.base_path().$file_path_for_download_exe.'&nid='.$node->nid;
                } else {
                  $file_download_path_exe = base_path().$file_path_for_download_exe;
                }
                
                $file_url_executive .= '<li>'.$img_disp.' <a href="'.$file_download_path_exe.'" title="'.$title.'"><b>Download Executive summary</b></a></li>';
                
              }
            }
            
            $node_files=$node->files;
            foreach($node_files as $file){
              $filedescription=$file->description;
              $filedate=date('F Y',$file->timestamp);
              $filepath=$file->filepath;
              $filepathname=explode('/',$filepath);
              $filepathname_disp=$filepathname[count($filepathname)-1];
              
              $file_type=explode('.',$filepath);
              $filetype_disp=trim(strtolower($file_type[count($file_type)-1]));
              
              if($filetype_disp=='pdf'){
                $img_disp='<img src="'.base_path().path_to_theme().'/images/doc_types/icon_'.$filetype_disp.'.gif" title="'.$title.'" alt="document_'.$filetype_disp.'">';
              }
              else if($filetype_disp=='doc' || $filetype_disp=='docx'){
                $change_type='doc';
                $img_disp='<img src="'.base_path().path_to_theme().'/images/doc_types/icon_'.$change_type.'.gif" title="'.$title.'" alt="document_'.$filetype_disp.'">';
              }
              else if($filetype_disp=='xls' || $filetype_disp=='xlsx'){
                $change_type='xls';
                $img_disp='<img src="'.base_path().path_to_theme().'/images/doc_types/icon_'.$change_type.'.gif" title="'.$title.'" alt="document_'.$filetype_disp.'">';
              }
              else if($filetype_disp=='ppt'){
                $change_type='ppt';
                $img_disp='<img src="'.base_path().path_to_theme().'/images/doc_types/icon_'.$change_type.'.gif" title="'.$title.'" alt="document_'.$filetype_disp.'">';
              }
              else {
                $change_type='generic';
                $img_disp='<img src="'.base_path().path_to_theme().'/images/doc_types/icon_'.$change_type.'.gif" title="'.$title.'" alt="document_'.$filetype_disp.'">';
              }
              
              $file_path_for_download = $file->filepath;
              
              if(!(stripos($file_path_for_download,'&'))){
                $file_download_path = base_path().drupal_get_path('module', 'pubdlcnt') .'/pubdlcnt.php?file='.base_path().$file_path_for_download.'&nid='.$node->nid;
              } else {
                $file_download_path = base_path().$file_path_for_download;
              }
              
              
              // $file_url .= '<li>'.$img_disp.' <a href="'.$file_download_path.'" title="'.$title.'"><b>Download File</b></a></li>';
              $file_url .= '<tr><td>'.++$i.'</td><td>'.$img_disp.' <a href="'.$file_download_path.'" title="'.$title.'">'.$filepathname_disp.'</a></td></tr>';
              
              }
              $i = 0;
              echo "<ul>";
              echo $file_url.$file_url_executive;
              
              if(!empty($field_file_location_page_url)){
                $genericImg = '<img src="'.base_path().path_to_theme().'/css/doc_types/icon_generic.gif" title="'.$title.'" alt="'.$title.'">';
                echo '<li>'.$genericImg." ".$file_page_url.'</li>';
              }
              
              
              echo "</ul>";
            ?>
            </table>
          </div>
            
        </div>  
        <div class="node_fields">
          <?php
            $imageNameVal = $node->field_image[0]['filename'];
            $file_path = $node->field_image[0]['filepath'];
            
            //Here is the changes for the alt and title attribute of image
            $imageAltTag = $node->field_image[0]['data']['alt'];
            if(!empty($imageAltTag)){
              $alt = $title = $imageAltTag;
            } else {
              $alt = $title;
            }
            
            if(!empty($imageNameVal)){
          ?>
          <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4">
              <div class="node_image text-center">
                <?php              
                  print theme('imagecache', 'node_page_image', $file_path, $alt, $title);
                ?>
              </div>
              <?php if ($terms): ?>       
                <div class="taxonomy">
                  <?php
                        print display_cea_terms($node, $vid = NULL, $unordered_list = true);
                  ?>
                  <div style="clear:both"></div>
                </div>       
              <?php endif;?>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-8">
              <div class="body_txt">
                <?php print $node->content['body']['#value'];?>
              </div>
            </div>
          </div>
          <div style="clear:both"></div>
          <?php
            } else {
          ?>
          <div class="body_txt">
            <?php print $node->content['body']['#value'];?>
          </div>
          <div style="clear:both"></div>
          <?php if ($terms): ?>       
            <div class="taxonomy">
              <?php
                    print display_cea_terms($node, $vid = NULL, $unordered_list = true);
              ?>
              <div style="clear:both"></div>
            </div>       
          <?php endif;?>  
          <?php
            }
          ?>
        </div>
        
       </div>
      
     <div class="page_links">
          <?php if ($links): ?> 
            <div class="links"> <?php print $links; ?></div>
          <?php endif; ?>
     </div>
     
     <!--<div class="node_navigate_links">
      <a href="javascript:void();" onclick="javascript:history.go(-1)" title="Back to List">Back</a>  |
      <a href="<?php print base_path();?>library" title="Custom resources query">Custom resources query</a> |
      <a href="<?php print base_path();?>" title="Site home">Site home</a>
     </div>-->
  </div> <!-- /node-inner -->
  
  <?php /* <div class="print_Ver">        
    <?php print l('<img src="'.base_path().path_to_theme().'/css/images/print_icon.gif
                  " title="Printer-friendly version" alt="Printer-friendly version" class="imgCls" />Printer-friendly version', "print/".$node->nid, array('html' => true, 'attributes' => array('target' => '_blank')));?>
  </div> */ ?>
  </div>
  
</div> <!-- /node-->

