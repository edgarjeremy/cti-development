<?php
/**
 * @file views-view-list.tpl.php
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>

<section >
  <div class="entry-header">
    <?php if (!empty($title)) : ?>
      <h2><?php print $title; ?></h2>
    <?php endif; ?>
    <hr>
  </div>
  <div class="entry-content">
    <div  class="item-list">
      <<?php print $options['type']; ?>>
        <?php foreach ($rows as $id => $row): ?>
          <li class="<?php print $classes[$id]; ?>"><?php print $row; ?></li>
        <?php endforeach; ?>
      </<?php print $options['type']; ?>>
    </div>
  </div>
</section>
