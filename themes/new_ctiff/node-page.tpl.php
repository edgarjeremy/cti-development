<?php if($node->title == "404" || $node->title == "403"): 
  $rootheme = check_url($front_page) .'sites/all/themes/new_ctiff';
?>
  <div class='container'>
    <div class='page-not-found'>
      <img alt='' src='<?php echo $rootheme ?>/images/section/404-pic.png'>
      <div class='text'>
        <span class='sorry'>
          Sorry!
        </span>
        <span class='detail'>
          The page you're looking for cannot be found
        </span>
      </div>
      <div class='search'>
        <form class="" id="search-form" method="post" accept-charset="UTF-8" action="/search/">
          <div class='form-group'>
            <input name="keys" id="edit-keys" placeholder="Enter your keyword here" type="text">
          </div>
        </form>
      </div>
    </div>
  </div>
<?php elseif($node->title == "Subscribe"): ?>
<?php else: ?>
  <?php
    global $base_url;
    $uri = $node->path;
  //  var_dump($node->path); exit;
    $breadcrumb = array();
    $breadcrumb[] = l('Home', '');
    $target_link = "";
    foreach(explode('/', $uri) as $key => $link) {
      $target_link .= '/' . $link;
      $breadcrumb[] = l(ucfirst($link), $base_url . $target_link);
    }
    array_pop($breadcrumb);
    $breadcrumb[] = l(drupal_get_title(), $base_url . base_path() . $uri);

    // Set Breadcrumbs
    $breadcrumbs = drupal_set_breadcrumb($breadcrumb);
    ?>
  <div class='container'>
    <section class='fullwidth-page'>
      <div class='entry-header'>
        <div class="breadcrumb">
          <ol class="breadcrumb">
            <?php foreach($breadcrumbs as $breadcrumb): ?>
              <li><?php echo $breadcrumb; ?></li>
            <?php endforeach; ?>
          </ol>
        </div>
        <h1><?php print $title ?></h1>      
      </div>
      <?php if(!empty($content)) : ?>
      <div class='entry-content'>
        <?php print $content ?>
      </div>
      <?php endif;?>
    </section>
  </div>
<?php endif; ?>