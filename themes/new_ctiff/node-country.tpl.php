<?php
  global $base_url;
  $uri = $node->path;

  $breadcrumb = array();
  $breadcrumb[] = l('Home', '');
  $target_link = "";
  foreach(explode('/', $uri) as $key => $link) {
    $target_link .= '/' . $link;
    $breadcrumb[] = l(ucfirst($link), $base_url . $target_link);
  }
  array_pop($breadcrumb);
  $breadcrumb[] = l(drupal_get_title(), $base_url . base_path() . $uri);

  // Set Breadcrumbs
  $breadcrumbs = drupal_set_breadcrumb($breadcrumb);
  ?>
  <section class='fullwidth-page'>
    <div class='entry-header'>
      <div class="breadcrumb">
        <ol class="breadcrumb">
          <?php foreach($breadcrumbs as $breadcrumb): ?>
            <li><?php echo $breadcrumb; ?></li>
          <?php endforeach; ?>
        </ol>
      </div>
      <h1><?php print $title ?></h1>      
    </div>
    <?php if(!empty($content)) : ?>
    <div class='entry-content'>
      <?php print $content ?>
    </div>
    <?php endif;?>
  </section>