<?php
$result=$view->result;
foreach($result as $val){
    
    $node=node_load($val->nid);
    $node_title=(strlen($node->title)>25)?substr($node->title,0,25)."...":$node->title;
    $node_teaser=(strlen($node->body)>100)?substr($node->body,0,100)."...":$node->body;
    $node_image=$node->field_related_image[0]['filepath'];
    //print theme('imagecache', 'front_banner', base_path().$node_image, $node_title, '');
    $link=explode("/",$node->path);
    $link_string="<a href='/".$link[1]."/".$link[2]."'>".$node_title."</a>";
    $description = "<h2>".$link_string."</h2>";
    $description .= "<div>".$node_teaser."</div>";
    $rsize_image_path=imagecache_create_url('community_rotate',$node_image);
    //print $rsize_image;
    $pic_array[]='["'.$rsize_image_path.'","","","'.$description.'"]';
    //dpr($val);
    
}
?>
<script src="<?php print $base_path . path_to_theme() ?>/js/simplegallery.js" type="text/javascript"></script>

<script type="text/javascript">

var mygalleryr=new simpleGallery({
	wrapperid: "simplegallery", //ID of main gallery container,
	dimensions: [695, 310], //width/height of gallery in pixels. Should reflect dimensions of the images exactly
	imagearray: [<?php print implode(",",$pic_array) ?>],
	autoplay: [true, 10000, 9500], //[auto_play_boolean, delay_btw_slide_millisec, cycles_before_stopping_int]
	persist: false, //remember last viewed slide and recall within same session?
	fadeduration: 1000, //transition duration (milliseconds)
	oninit:function(){ //event that fires when gallery has initialized/ ready to run
		//Keyword "this": references current gallery instance (ie: try this.navigate("play/pause"))
	},
	onslide:function(curslide, i){ //event that fires after each slide is shown
		//Keyword "this": references current gallery instance
		//curslide: returns DOM reference to current slide's DIV (ie: try alert(curslide.innerHTML)
		//i: integer reflecting current image within collection being shown (0=1st image, 1=2nd etc)
	}
})
var img_slide_path='<?php print base_path().path_to_theme() ?>';
</script> 

  
<div id="simplegallery"></div>