<?php header("Cache-Control: no-cache, must-revalidate");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

  <head>
    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <!--[if lte IE 6]><style type="text/css" media="all">@import "<?php print $base_path . path_to_theme() ?>/css/ie6.css";</style><![endif]-->
    <!--[if IE 7]><style type="text/css" media="all">@import "<?php print $base_path . path_to_theme() ?>/css/ie7.css";</style><![endif]-->
     <!--[if IE 8]><style type="text/css" media="all">@import "<?php print $base_path . path_to_theme();?>/css/ie8.css";</style><![endif]-->
    <?php print $scripts; ?>    
    
    <link rel="stylesheet" type="text/css" media="print" href="<?php print $base_path . path_to_theme();?>/css/print.css" />

    <script src="<?php print $base_path . path_to_theme() ?>/js/cti.js" type="text/javascript"></script>
  </head>

  <body class="cea-page <?php print $body_classes; ?>">
   <div style="display:none">
      <a href="#skipnavigation"></a>
    </div>  
    
    <div id="page">

    <!-- ______________________ HEADER _______________________ -->

    <div id="header">
      <div id="logo-title">	    
        <?php if (!empty($logo)): ?>
          <a href="<?php print base_path();?>" title="<?php print $site_slogan; ?>" rel="home" id="logo">
          <img src="<?php print base_path().path_to_theme();?>/cti_logo.png" title="<?php print $site_slogan; ?>" alt="<?php print $site_slogan; ?>"/> 
	      </a>
        <?php endif; ?>
          
      </div> <!-- /logo-title -->
      <div id="site-header">
	<div class="site_search">
	    <div class="div_border">
	    <form class="search-form" id="search-form" method="post" accept-charset="UTF-8" action="<?php print base_path();?>search/">
		<!--div class="search_label">
		    Search Content
		</div-->
		<div id="search_cti_field" class="search_cti_box">
		    <div class="search_field_box">
			<input type="text" class="form-text search_cti" value="Search" id="edit-keys" name="keys"/>
		    </div>
		    <div class="search_field_button">
			<input id="edit-search" class="search-form-submit" name="op" type="submit" value="" />
		
		    </div>
		</div>
		<div class="search_cti_box">
		</div>
	    </form>
	    </div>
	  </div>
         <?php print $header_right; ?>
      </div> <!-- /site header -->
      <div class="clear_all"></div>
    </div> <!-- /header -->
    <a name="skipnavigation" title="skip navigation"></a>
    <!-- _________________________Nice Menus__________________________ -->
    
    <?php
      if (!empty($nice_menu)):
    ?>
      <div id="nice_menu">
	<div  class="nice_menu_links">
	    <?php print $nice_menu; ?>
	    <div class="clear_all"></div>
	</div>
      </div>
    <?php
      endif;
    ?>
    
    
    <!-- ______________________ MAIN _______________________ -->

    <div id="main" class="clearfix">
        
        <div id="sidebar-first" class="column sidebar first">
	 
	  
	  <div id="sidebar-first-inner" class="inner">                
	      <?php print $left; ?>                
	  </div>
	  
	  <div class="clear_all"></div>
        </div>
        
        <div id="content" class="contentRight">
	  
	  <!--This has contained with the main middle part -->
	    <div class="bottomHighLightContainer">                    
		<div class="bottomLeftData">
		    <div class="div_border">
			
			  <?php if ($content_top): ?>
			    <div id="content-top" style="float:left;">
			      <?php print $content_top; ?>
			    </div> <!-- /#content-top -->
			  <?php endif; ?>
			
			<div class="clear_all"></div>
		    </div>
		    
		</div>                         
		
	       <div class="clear_all"></div>
	      <!-- End of the bottomHighLightContainer -->
	    </div>
	    
	  
	
	    <div class="common secondMiddleContainer">  <!--This has contained with the extrem top part -->
		<div class="div_border">
		    <div id="homepage_right_top_a">
		      <?php print $homepage_right_top_a; ?>
		      <!-- homepage_right_top_a -->	
		    </div>
		  
		    <div id="homepage_right_top_b">
		      <?php print $homepage_right_top_b; ?>
		      <!-- homepage_right_top_b -->
		    </div>
		  
		    <div id="homepage_right_top_map">
		      <?php print $homepage_right_top_map; ?>
		      <div id="browse_resource_by_country">
		      <?php
		        $Browse_Resources_by_Country = "<a href='".base_path()."node/6413'>Browse Resources by Country (Disability-accessible)</a>";
		        print $Browse_Resources_by_Country;
		      ?>
		      </div>
		      <!-- homepage_right_top_map -->
		    </div>
	
		    <div class="clear_all"></div>
		</div>
		
	    </div><!-- End of secondMiddleContainer -->
	    
	    <div class="thirdMiddleContainder">
		<div id="bottom_rotor_container">
		  <div class="div_border">
		    <?php print $content_bottom; ?>
		    <div class="clear_all"></div>
		  </div>
		
	    </div><!-- homepage_extrem_top -->
		
	    
	    
                <div class="clear_all"></div>
            </div><!-- content-inner -->
	    
	    <div class="fourthMiddleContainder">
		<div id="bottom_rotor_container">
		  <div class="div_border">
		     <?php print $homepage_content_bottom; ?>
		    
		    	    <div class="clear_all"></div>
		  </div>
		
	    </div><!-- homepage_extrem_top -->
		
	    
	    
                <div class="clear_all"></div>
            </div>
	    
        </div> <!-- content -->
    </div> <!-- /main -->

      <!-- ______________________ FOOTER _______________________ -->

      
        <div id="footer">
           <!-- <div id="footer_left" class="footer_layout">
            </div>-->
            <div id="footer_body" class="footer_layout">                
                <?php print $footer_block; ?>
            </div>
           <!-- <div id="footer_right" class="footer_layout">
            </div>-->
            <div class="clear_all"></div>
			<div id="footer_message">
			  <?php print $footer_message; ?>
			</div>
      </div> <!-- /footer -->
      

    </div> <!-- /page -->
    <?php print $closure; ?>
  </body>
</html>
