<?php
    
// Auto-rebuild the theme registry during theme development.
if (theme_get_setting('cti_rebuild_registry')) {
  drupal_rebuild_theme_registry();
}

// Add Zen Tabs styles
if (theme_get_setting('basic_zen_tabs')) {
  drupal_add_css( drupal_get_path('theme', 'basic') .'/css/tabs.css', 'theme', 'screen');
}

/*
 *	 This function creates the body classes that are relative to each page
 *	
 *	@param $vars
 *	  A sequential array of variables to pass to the theme template.
 *	@param $hook
 *	  The name of the theme function being called ("page" in this case.)
 */
//function cti_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
//  /*echo "<pre>";
//  print_r($link);
//  echo "</pre>";*/
// // echo $menu.'c';
//  //echo "dassdsdads";
//  echo $link;
//      $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
//      if (!empty($extra_class)) {
//        $class .= ' '. $extra_class;
//      }
//      if ($in_active_trail) {
//        $class .= ' active-trail';
//      }
//       //$class .= ' active-trail';
//     // New line added to get unique classes for each menu item using form_clean_id() to clean it up
//      $css_class = form_clean_id(strip_tags(strtolower($link)));
//     // $link=str_replace('<a id="dhtml_menu-1527" class="active-trail active" href="/about-us">ABOUT CTI</a>',' class=\"active-trail active\"',$link);
//      //$menu=str_replace('<a','',$link);
//      return '<li class="'. $class . ' ' . $css_class . '">' . $link . $menu ."</li>\n";
//    }

function cti_preprocess_page(&$vars, $hook) {
   
  $vars['head']=str_replace('<link rel="alternate" type="application/rss+xml" title="Coral Triangle Initiative RSS" href="http://beta.coraltriangleinitiative.org/rss.xml" />','',$vars['head']);
    
}

/*
 *	 This function creates the NODES classes, like 'node-unpublished' for nodes
 *	 that are not published, or 'node-mine' for node posted by the connected user...
 *	
 *	@param $vars
 *	  A sequential array of variables to pass to the theme template.
 *	@param $hook
 *	  The name of the theme function being called ("node" in this case.)
 */

function cea_preprocess_node(&$vars, $hook) {
  // Special classes for nodes
  $classes = array('node');
  if ($vars['sticky']) {
    $classes[] = 'sticky';
  }
  // support for Skinr Module
  if (module_exists('skinr')) {
    $classes[] = $vars['skinr'];
  }
  if (!$vars['status']) {
    $classes[] = 'node-unpublished';
    $vars['unpublished'] = TRUE;
  }
  else {
    $vars['unpublished'] = FALSE;
  }
  if ($vars['uid'] && $vars['uid'] == $GLOBALS['user']->uid) {
    $classes[] = 'node-mine'; // Node is authored by current user.
  }
  if ($vars['teaser']) {
    $classes[] = 'node-teaser'; // Node is displayed as teaser.
  }
  $classes[] = 'clearfix';
  
  // Class for node type: "node-type-page", "node-type-story", "node-type-my-custom-type", etc.
  $classes[] = cea_id_safe('node-type-' . $vars['type']);
  $vars['classes'] = implode(' ', $classes); // Concatenate with spaces
}

/*
 *	This function create the EDIT LINKS for blocks and menus blocks.
 *	When overing a block (except in IE6), some links appear to edit
 *	or configure the block. You can then edit the block, and once you are
 *	done, brought back to the first page.
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called ("block" in this case.)
 */ 

function cea_preprocess_block(&$vars, $hook) {
    $block = $vars['block'];
    // special block classes
    $classes = array('block');
    $classes[] = cea_id_safe('block-' . $vars['block']->module);
    $classes[] = cea_id_safe('block-' . $vars['block']->region);
    $classes[] = cea_id_safe('block-id-' . $vars['block']->bid);
    $classes[] = 'clearfix';
    
    // support for Skinr Module
    if (module_exists('skinr')) {
      $classes[] = $vars['skinr'];
    }
    
    $vars['block_classes'] = implode(' ', $classes); // Concatenate with spaces

    if (theme_get_setting('basic_block_editing') && user_access('administer blocks')) {
        // Display 'edit block' for custom blocks.
        if ($block->module == 'block') {
          $edit_links[] = l('<span>' . t('edit block') . '</span>', 'admin/build/block/configure/' . $block->module . '/' . $block->delta,
            array(
              'attributes' => array(
                'title' => t('edit the content of this block'),
                'class' => 'block-edit',
              ),
              'query' => drupal_get_destination(),
              'html' => TRUE,
            )
          );
        }
        // Display 'configure' for other blocks.
        else {
          $edit_links[] = l('<span>' . t('configure') . '</span>', 'admin/build/block/configure/' . $block->module . '/' . $block->delta,
            array(
              'attributes' => array(
                'title' => t('configure this block'),
                'class' => 'block-config',
              ),
              'query' => drupal_get_destination(),
              'html' => TRUE,
            )
          );
        }
        // Display 'edit menu' for Menu blocks.
        if (($block->module == 'menu' || ($block->module == 'user' && $block->delta == 1)) && user_access('administer menu')) {
          $menu_name = ($block->module == 'user') ? 'navigation' : $block->delta;
          $edit_links[] = l('<span>' . t('edit menu') . '</span>', 'admin/build/menu-customize/' . $menu_name,
            array(
              'attributes' => array(
                'title' => t('edit the menu that defines this block'),
                'class' => 'block-edit-menu',
              ),
              'query' => drupal_get_destination(),
              'html' => TRUE,
            )
          );
        }
        // Display 'edit menu' for Menu block blocks.
        elseif ($block->module == 'menu_block' && user_access('administer menu')) {
          list($menu_name, ) = split(':', variable_get("menu_block_{$block->delta}_parent", 'navigation:0'));
          $edit_links[] = l('<span>' . t('edit menu') . '</span>', 'admin/build/menu-customize/' . $menu_name,
            array(
              'attributes' => array(
                'title' => t('edit the menu that defines this block'),
                'class' => 'block-edit-menu',
              ),
              'query' => drupal_get_destination(),
              'html' => TRUE,
            )
          );
        }
        $vars['edit_links_array'] = $edit_links;
        $vars['edit_links'] = '<div class="edit">' . implode(' ', $edit_links) . '</div>';
      }
  }

/*
 * Override or insert PHPTemplate variables into the block templates.
 *
 *  @param $vars
 *    An array of variables to pass to the theme template.
 *  @param $hook
 *    The name of the template being rendered ("comment" in this case.)
 */

function cea_preprocess_comment(&$vars, $hook) {
  // Add an "unpublished" flag.
  $vars['unpublished'] = ($vars['comment']->status == COMMENT_NOT_PUBLISHED);

  // If comment subjects are disabled, don't display them.
  if (variable_get('comment_subject_field_' . $vars['node']->type, 1) == 0) {
    $vars['title'] = '';
  }

  // Special classes for comments.
  $classes = array('comment');
  if ($vars['comment']->new) {
    $classes[] = 'comment-new';
  }
  $classes[] = $vars['status'];
  $classes[] = $vars['zebra'];
  if ($vars['id'] == 1) {
    $classes[] = 'first';
  }
  if ($vars['id'] == $vars['node']->comment_count) {
    $classes[] = 'last';
  }
  if ($vars['comment']->uid == 0) {
    // Comment is by an anonymous user.
    $classes[] = 'comment-by-anon';
  }
  else {
    if ($vars['comment']->uid == $vars['node']->uid) {
      // Comment is by the node author.
      $classes[] = 'comment-by-author';
    }
    if ($vars['comment']->uid == $GLOBALS['user']->uid) {
      // Comment was posted by current user.
      $classes[] = 'comment-mine';
    }
  }
  $vars['classes'] = implode(' ', $classes);
  
  
}

/* 	
 * 	Customize the PRIMARY and SECONDARY LINKS, to allow the admin tabs to work on all browsers
 * 	An implementation of theme_menu_item_link()
 * 	
 * 	@param $link
 * 	  array The menu item to render.
 * 	@return
 * 	  string The rendered menu item.
 */ 	

function cea_menu_item_link($link) {
  if (empty($link['localized_options'])) {
    $link['localized_options'] = array();
  }

  // If an item is a LOCAL TASK, render it as a tab
  if ($link['type'] & MENU_IS_LOCAL_TASK) {
    $link['title'] = '<span class="tab">' . check_plain($link['title']) . '</span>';
    $link['localized_options']['html'] = TRUE;
  }

  return l($link['title'], $link['href'], $link['localized_options']);
}


/*
 *  Duplicate of theme_menu_local_tasks() but adds clear-block to tabs.
 */

function cea_menu_local_tasks() {
  $output = '';
  if ($primary = menu_primary_local_tasks()) {
    if(menu_secondary_local_tasks()) {
      $output .= '<ul class="tabs primary with-secondary clearfix">' . $primary . '</ul>';
    }
    else {
      $output .= '<ul class="tabs primary clearfix">' . $primary . '</ul>';
    }
  }
  if ($secondary = menu_secondary_local_tasks()) {
    $output .= '<ul class="tabs secondary clearfix">' . $secondary . '</ul>';
  }
  return $output;
}

/* 	
 * 	Add custom classes to menu item
 */	
	
function cea_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
  $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
  if (!empty($extra_class)) {
    $class .= ' '. $extra_class;
  }
  if ($in_active_trail) {
    $class .= ' active-trail';
  }
#New line added to get unique classes for each menu item
  $css_class = cea_id_safe(str_replace(' ', '_', strip_tags($link)));
  return '<li class="'. $class . ' ' . $css_class . '">' . $link . $menu ."</li>\n";
}

/*	
 *	Converts a string to a suitable html ID attribute.
 *	
 *	 http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 *	 valid ID attribute in HTML. This function:
 *	
 *	- Ensure an ID starts with an alpha character by optionally adding an 'n'.
 *	- Replaces any character except A-Z, numbers, and underscores with dashes.
 *	- Converts entire string to lowercase.
 *	
 *	@param $string
 *	  The string
 *	@return
 *	  The converted string
 */	

function cea_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id'. $string;
  }
  return $string;
}

/*
 *  Return a themed breadcrumb trail.
 *	Alow you to customize the breadcrumb markup
 */

function cea_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' » ', $breadcrumb) .'</div>';
  }
}

/* Function for displaying the array*/
function pr_disp($arg){
  echo '<pre>';
  print_r($arg);
  echo '</pre>';
}

function _substr($str, $length, $minword = 3){
    $sub = '';
    $len = 0;
   
    foreach (explode(' ', $str) as $word){
        $part = (($sub != '') ? ' ' : '') . $word;
        $sub .= $part;
        $len += strlen($part);
       
        if (strlen($word) > $minword && strlen($sub) >= $length){
            break;
        }
    }
   
    return $sub . (($len < strlen($str)) ? '...' : '');
}


/**
 * Rewrite the terms links to custom view
 */

function display_cea_terms($node, $vid = NULL, $ordered_list = TRUE) {
     
      //Count the number of taxonomy for each node
      $tax_data=$node->taxonomy;     
      $total_tax=count($tax_data);
     
      $vocabularies = taxonomy_get_vocabularies();
      $iv=0;
      $tot_v=count($vocabularies);
       
      //if ($ordered_list) $output .= '<ul>'; //checks to see if you want an ordered list
      if ($vid) { //checks to see if you've passed a number with vid, prints just that vid
        $output = '<div class="tags-'. $vid . '">';
        foreach($vocabularies as $vocabulary) {
         if ($vocabulary->vid == $vid) {
           $terms = taxonomy_node_get_terms_by_vocabulary($node, $vocabulary->vid);
           if ($terms) {
             $links = array();
             $output .= '<span class="only-vocabulary-'. $vocabulary->vid . '">';
             if ($ordered_list) $output .= '<li class="vocabulary-'. $vocabulary->vid . '">';
             
             foreach ($terms as $term) {
               if($vocabulary->name=='Resource Type'){
                    $term_path='resource_by_type/'.$term->tid;
               }
               else if($vocabulary->name=='Primary Subject'){
                    $term_path='resource_by_subject/'.$term->tid;
               }
	       else if($vocabulary->name=='Secondary Subject'){
                    $term_path='resource_by_subject/'.$term->tid;
               }
               else if($vocabulary->name=='Country'){
                    $term_path='resource_by_country/'.$term->tid;
               }
               else if($vocabulary->name=='Events'){
                    $term_path='cea_event_type/'.$term->tid;
               }
		if($iv<$total_tax-1){
		  $links[] = '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description))) .' | </ li>';
		}
		else{
		  $links[] = '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description))) .'</ li>';
		}
		$iv++;             }
	      $output .= implode('  ', $links);
	      if ($ordered_list) $output .= '</li>';
	      $output .= '</span>';
	    }
	  }
	}
      }
      else {
       //$output = '<div class="taxonomy">';
       $term_psubject1=array();
       foreach($vocabularies as $vocabulary) {
         if ($vocabularies) {
           $terms = taxonomy_node_get_terms_by_vocabulary($node, $vocabulary->vid);
           if ($terms) {
             $links = array();
             if ($ordered_list) //$output .= '<li class="vocabulary-'. $vocabulary->vid . '">';
	     
             foreach ($terms as $k=>$term) {
	       
              if($vocabulary->name=='Type'){
                    $term_path='resource_by_type/'.$term->tid;
		    $term_type .= '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description)));
		    if(count($terms)!=$k+1)
		    {
		      $term_type.=' | ';
		    }
		     $term_type.='</li>';	
               }
			   
                if($vocabulary->name=='Subject'){
                    $term_path='resource_by_subject/'.$term->tid;
		    $term_psubject1[] = '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description)));

		     	    
               }
	      /* else if($vocabulary->name=='Secondary Subject'){
                    $term_path='resource_by_subject/'.$term->tid;
		    $term_psubject1[] = '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description)));
    
               }*/
               else if($vocabulary->name=='Country'){
                    $term_path='resource_by_country/'.$term->tid;
		    $term_country .= '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description)));
		    if(count($terms)!=$k+1)
		    {
		      $term_country.=' | ';
		    }
		     $term_country.='</li>';		    
               }
	       /*else if($vocabulary->name=='ECO Asia Event'){
                    $term_path='cea_event_type/'.$term->tid;
		    $term_events .= '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description)));
		    if(count($terms)!=$k+1)
		    {
		      $term_events.=' | ';
		    }
		     $term_events.='</li>';		    
               } */
             }
	      
             //$output .= implode('  ', $links);
	  }
	  
	}
	
      }

      if(count($term_psubject1)==1){
	$term_psubject=$term_psubject1[0].'</ li>';
      }
      elseif(count($term_psubject1)>1){
	$term_psubject=implode(' | </ li>',$term_psubject1).'</ li>';
      }
      
      
    }
      if($term_psubject){
       $term_subject_link ='<div class="term_format_link"><ul><li class="voc_name">Subject: </ li>'.$term_psubject.'</ul><div style="clear:both"></div></div>';
      }
      if($term_type){
       $term_type_link='<div class="term_format_link"><ul><li class="voc_name">Type: </ li>'.substr_replace($term_type,"",-7).'</ul><div style="clear:both"></div></div>';
      }
      if($term_country){
       $term_country_link='<div class="term_format_link"><ul><li class="voc_name">Country: </ li>'.substr_replace($term_country,"",-7).'</ul><div style="clear:both"></div></div>';
      }
     // if($term_events){
      // $term_event_link='<div class="term_format_link"><ul><li class="voc_name">Event: </ li>'.substr_replace($term_events,"",-7).'</ul><div style="clear:both"></div></div>';
     // }
      
      $output .=$term_subject_link.$term_type_link.$term_country_link.$term_event_link;
      
      return $output;
}


function display_cea_event_terms($node, $vid = NULL, $ordered_list = TRUE) {
     
      //Count the number of taxonomy for each node
      $tax_data=$node->taxonomy;     
      $total_tax=count($tax_data);
     
      $vocabularies = taxonomy_get_vocabularies();
      $iv=0;
      $tot_v=count($vocabularies);
       
      //if ($ordered_list) $output .= '<ul>'; //checks to see if you want an ordered list
      if ($vid) { //checks to see if you've passed a number with vid, prints just that vid
        $output = '<div class="tags-'. $vid . '">';
        foreach($vocabularies as $vocabulary) {
         if ($vocabulary->vid == $vid) {
           $terms = taxonomy_node_get_terms_by_vocabulary($node, $vocabulary->vid);
           if ($terms) {
             $links = array();
             $output .= '<span class="only-vocabulary-'. $vocabulary->vid . '">';
             if ($ordered_list) $output .= '<li class="vocabulary-'. $vocabulary->vid . '">';
             
             foreach ($terms as $term) {
               if($vocabulary->name=='Resource Type'){
                    $term_path='resource_by_type/'.$term->tid;
               }
               else if($vocabulary->name=='Primary Subject'){
                    $term_path='resource_by_subject/'.$term->tid;
               }
	       else if($vocabulary->name=='Secondary Subject'){
                    $term_path='resource_by_subject/'.$term->tid;
               }
               else if($vocabulary->name=='Country'){
                    $term_path='resource_by_country/'.$term->tid;
               }
               else if($vocabulary->name=='Events'){
                    $term_path='cea_event_type/'.$term->tid;
               }
		if($iv<$total_tax-1){
		  $links[] = '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description))) .' | </li>';
		}
		else{
		  $links[] = '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description))) .'</li>';
		}
		$iv++;             }
	      $output .= implode('  ', $links);
	      if ($ordered_list) $output .= '</li>';
	      $output .= '</span>';
	    }
	  }
	}
      }
      else {
       //$output = '<div class="taxonomy">';
       foreach($vocabularies as $vocabulary) {
         if ($vocabularies) {
           $terms = taxonomy_node_get_terms_by_vocabulary($node, $vocabulary->vid);
           if ($terms) {
             $links = array();
             //$output .= '<ul class="vocabulary-'. $vocabulary->vid . '">';
             if ($ordered_list) //$output .= '<li class="vocabulary-'. $vocabulary->vid . '">';
	     
             foreach ($terms as $term) {
               if($vocabulary->name=='ECO Asia Event'){
                    $term_path='cea_event_type/'.$term->tid;
		    $term_events .= '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description))) .' | </li>';
               }       	       
             }
             //$output .= implode('  ', $links);
	  }
	  
	}
	
      }
    }
     //if ($ordered_list) $output .= '</ul>';
     
      $term_event_link ='<div class="term_format_link"><ul><li class="voc_name">Events: </li>'.$term_events.'</ul><div style="clear:both"></div></div>';
      
      $output .= $term_event_link;
      
      return $output;
}

/*****Funtion to use in most download count block of document******/
function most_downloaded_block_document() { 
  global $user;
  $header[] = array('data' => t('filename'), 'field' => 'filename');
  $header[] = array('data' => t('hits'), 'field' => 'count', 'sort' => 'desc');
  $header[] = array('data' => t('last download'), 'field' => 'timestamp');
  $header[] = array('data' => t('action'));
  $category_list=array();
  $category_path=array();
  $path = explode('/',drupal_get_path_alias($_GET['q']));
	$tag_list=array();
  $tag_path=array();
  $fileDirectoryPath = file_directory_path() . '/';  
  $counter=0;
	$timg = '<img src="'.base_path().path_to_theme().'/css/images/term.gif" /> ';
	
  $sql = "SELECT fd.filename, fd.count, fd.timestamp, u.nid, nt.name FROM {file_downloads} fd JOIN {files} f ON f.filepath = CONCAT('$fileDirectoryPath', fd.filename) JOIN {upload} u ON u.fid = f.fid JOIN {node} n ON n.nid = u.nid JOIN {node_type} nt ON nt.type = n.type"
	  . tablesort_sql($header);
  $count_query = "SELECT COUNT(*) FROM {file_downloads} fd JOIN {files} f ON f.filepath = CONCAT('$fileDirectoryPath', fd.filename) JOIN {upload} u ON u.fid = f.fid JOIN {node} n ON n.nid = u.nid JOIN {node_type} nt ON nt.type = n.type"
	  . tablesort_sql($header);
  $output = '<div class="user-course-files">';
  $result= pager_query($sql, 10, 0, $count_query);  
	if($result != "") {
		while($nid = db_fetch_object($result)){
		$node=node_load($nid->nid);
		//print $nid->nid;
		if($path[1]){
			$flag = true;
			if(!is_numeric($path[1])){
				$ptaxo = explode(",",$path[1]);
				$pc = 0;
				foreach($node->taxonomy as $taxo){ //print $taxo->tid.'[';
					if($taxo->tid==$ptaxo[0] || $taxo->tid==$ptaxo[1]){
						$flag = true;
						$pc++;
					}
					else {
						$flag = false;
					}
				}
				$flag = $pc==2?true:false;
			}
			else{
				foreach($node->taxonomy as $taxo){ //print $taxo->tid.'[';
					if($taxo->tid==$path[1]){
						$flag = true;
						break;
					}
					else {
						$flag = false;
					}
				}
			}
			if(!$flag){
				continue;
			}
		}
		$vid = array();
		$toutput = '';
		$term_data = db_query("SELECT {term_data}.tid as tid, {term_data}.vid as vid, {term_data}.name as name FROM {term_data}, {term_node} WHERE {term_node}.tid = {term_data}.tid AND {term_node}.nid =%d AND {term_node}.vid =%d ORDER BY {term_data}.vid",$node->nid,$node->vid);
		$n = 0;
		while($tresult = db_fetch_object($term_data)){
			$tpath = taxonomy_term_path($tresult->name).$tresult->tid;
			$term_path=drupal_get_path_alias($tpath);
			if(!in_array($tresult->vid,$vid)){
				$br = $n?'<br />':'';
				$vname  = db_result(db_query("SELECT name FROM {vocabulary} WHERE vid = %d",$tresult->vid));
				$toutput .= $br.$timg.$vname.': <a href="'.$term_path.'">'.$tresult->name.'</a>';
				$vid[]  = $tresult->vid;
				$n++;
			}
			else {
				$toutput .= ', <a href="'.$term_path.'">'.$tresult->name.'</a>';
			}
		}
		$fresult = db_query("SELECT `fid` FROM {upload} WHERE `nid` = %d AND `vid` = %d",$node->nid,$node->vid);
		
		while($files = db_fetch_object($fresult)){
			$fname = db_result(db_query("SELECT `filename` FROM {files} WHERE `fid` = %d",$files->fid));  
			$ftype = crc_file_type($fname);
			$npath = base_path().drupal_get_path_alias('node/'.$node->nid);
			$user_data = user_profile_name($node->uid);
			$img = '<img src="'.base_path().path_to_theme().'/css/images/icon-'.$ftype->type.'.png" class="c-u-file" align="absmiddle" /> ';
					
			$output .= '<div id="content-data">tttttt
				<div style="float:left;width:42px;">'.$img.'</div>
				<div id="download_body" style="float:left;">
					<div class="title"><a href="'.$npath.'">'.ucfirst($node->title).'</a></div>Posted by '.$user_data->fname.' on '.date("M d, Y",$node->changed).'  
					<div class="doc-cat">'.$toutput.'</div>
				</div>
			</div>
			<div class="clear-both" style="padding:3px 2px;"><br /></div>';
	
			}
		}
  }
  $output .= '<div style="clear:both"></div></div>';
	$content = $output;
  $content .= theme('pager', NULL, 10);
  return $content;
 //  return $output;
}


function cea_date_all_day_label() {
/** This overrides a theme found in the date module so that "All Day" will not display on the calendar.
To return to normal function replace the line with return t('(All day)'); or any other text that you would rather display for an all day event. Might be better to override the whole them function rather than the label but this is an easy fix **/
return t('&nbsp;');
}


/* This function has been created to check the file type and to get the image name as per the file type */
  function getImageNameWithFileExtenstion($fileName){
    $imageName = '';  
    $fileExnt = explode(".",$fileName);
    $total = count($fileExnt);
    $fileExtention = $fileExnt[$total - 1];
    $fileExtention = trim(strtolower($fileExtention));
    switch($fileExtention){
      case 'pdf':
	$imageName = "pdf";
	$altTag = "CEA pdf document";
	break;
    
      case 'doc':
      case 'docx':
      case 'rtf':
	$imageName = "word";
	$altTag = "CEA word document";
	break;
    
      default:
	$imageName = "fancyDocument";
	$altTag = "CEA document";
	break;
    }
    return $imageName;
  }
  
  // To make the search word bold  
  function emphasize_specific_words($str, $word, $type = 'highlight'){
      if($type == 'highlight'){
          $var_style = 'background: #CEDAEB;';
      } else if($type == 'bold') {
          $var_style = 'font-weight: bold;';
      } else if($type == 'underline') {
          $var_style = 'text-decoration: underline;';
      }
      
      if($word){
          $word= strip_tags($word);
          $word= preg_quote($word, '/');
          
          $str = preg_replace('/'.$word.'/i', "<span style='".$var_style."'>".$word."</span>",$str);
      }
      return $str;
  }
  
  
  //This function has been written for the pagination of Resource by country   
  function pagination_custom($rowsPerPage=20, $numrows, $country_arr, $pageKey, $countryKey, $nodeKey, $tabId = ''){
      $pageNum=1;
      $totalNode = 0;
      $country = 69;
      $indexValue = 0;
      $keys = array_keys($country_arr);
      $countryFirstId = $keys[0];
      $firstIndex = 0;
      if($tabId != ''){
	$tabId = "&".$tabId;
      }
      
      if(isset($_GET[$pageKey])){
	$pageNum=trim(mysql_escape_string($_GET[$pageKey]));
      }
      $maxPage=ceil($numrows/$rowsPerPage);
     
      $self=$_SERVER['REQUEST_URI'];
      $getUrl = explode('?',$self);
      $self = $getUrl[0];
      
      $mid = " ";
      $next = '';
      $prev = '';
      
      if($maxPage > 0){
	  $i = 1;
          
          
          foreach($country_arr as $country => $node_data){
           // echo $totalNode."<br/>";
           //echo count($node_data);
	    foreach($node_data as $indexValue => $nid ) {
	      $totalNode++;
            }
          }
         
          
          
          
	  foreach($country_arr as $country => $node_data){
           // echo $totalNode."<br/>";
          // echo count($node_data);
	    foreach($node_data as $indexValue => $nid ) {
	      $totalNode++;	      
	      if($totalNode > 19){
		  $totalNode = 0;
		  
		  $skipValueLess = $pageNum - 10;
		  $skipValueMore = $pageNum + 10;
		 // echo $pageNum.'--'.$i.'<br/>';
		  if($pageNum == $i){
		    $mid.= "<li class=\"pager-item\"><b>".$i."</b></li>";
		  }else if(($skipValueLess > $i) || ($skipValueMore < $i)){ //skip the near by pagination urls
		    
		  }else{		    
		    if($indexValue > 19){
		      $indexValue = intval($indexValue - 19);
		    } else {
		      $indexValue = 0;
		    }
		    $mid.= "<li class=\"pager-item\"><a href=\"$self?$pageKey=$i&$countryKey=$country&$nodeKey=$indexValue$tabId\" title=\"Go to page ".$i."\">".$i."</a></li>";		    
		  }
		  
		  /*if(($pageNum - 1) == $i){
		    if($indexValue > 9){
		      $indexValuePre =  $indexValue - 9;
		    } else {
		      $indexValuePre =  0;
		    }
		    
		    $prev = "<li class=\"pager-item\"><a href=\"$self?$pageKey=$i&$countryKey=$country&$nodeKey=$indexValuePre$tabId\" title=\"Go to prev page \">&laquo;Prev</a></li>";
		  }*/
		  
		  if(empty($prev)){
		    if(($pageNum -1) == $i){
		      $getCountry = '';
		      if(!empty($_GET['c'])){
			$getCountry = trim(mysql_escape_string($_GET['c']));
			
		      } else if(!empty($_GET['ct'])){
			$getCountry = trim(mysql_escape_string($_GET['ct']));
		      }
		      
		      if((!empty($getCountry)) && ($getCountry > $country)){
			$indexValuePre =  0;
			
		      } else {
			if($indexValue > 19){
			  $indexValuePre =  $indexValue - 19;
			} else {
			  $indexValuePre =  0;
			}
		      }
		      
		      $prev = "<li class=\"pager-item\"><a href=\"$self?$pageKey=$i&$countryKey=$country&$nodeKey=$indexValuePre$tabId\" title=\"Go to prev page \">&laquo;Prev</a></li>";
		    }
		  }
		  
		  if(empty($next)){
		    if(($pageNum + 1) == $i){
		      $getCountry = '';
		      if(!empty($_GET['c'])){
			$getCountry = trim(mysql_escape_string($_GET['c']));
			
		      } else if(!empty($_GET['ct'])){
			$getCountry = trim(mysql_escape_string($_GET['ct']));
		      }
		      
		      if((!empty($getCountry)) && ($getCountry < $country)){
			$indexValueNxt =  0;
		      } else {
			$indexValueNxt =  $indexValue;
		      }
		      
		      $next = "<li class=\"pager-item\"><a href=\"$self?$pageKey=$i&$countryKey=$country&$nodeKey=$indexValueNxt$tabId\" title=\"Go to next page \">Next&raquo;</a></li>";
		    }
		  }
		  
		  $i++;
		  $countryLastId = $country;
	      }
	    }
	  }
      }
      
      if($pageNum>1){
	  $page=$pageNum-1;
	  $first="<li class=\"pager-item\"><a href=\"$self?$pageKey=1&$countryKey=$countryFirstId&$nodeKey=$firstIndex$tabId\" title=\"Go to first page\">&laquo;first</a></li>";
      }else{
	  $first='<li class="pager-item">&laquo;<b>first</b></li>';
      }
       $page=$pageNum;
      // echo $page;
      if($pageNum<$i-1){
	  $page=$pageNum;
	  if($indexValue > 19){
	    $lastIndexKey = intval($indexValue - 19);
	  } else {
	    $lastIndexKey = 0;
	  }
          $i=$i-1;
	  $last="<li class=\"pager-item\"><a href=\"$self?$pageKey=$i&$countryKey=$countryLastId&$nodeKey=$lastIndexKey$tabId\" title=\"Go to last page\">last&raquo;</a></li>";
      }else{
	  $last='<li class="pager-item"><b>last</b>&raquo;</li>';
      }     
      
      
      
      $returnValue = '<ul class="pager">'.$first." ".$prev." ... ".$mid." ... ".$next." ".$last.'</ul>';
     //  $returnValue = '<ul class="pager">'.$first."  ". ... ".$mid." ... ."  ".$last.'</ul>';   
      
      return $returnValue;
  }
    
    
    function display_cti_document_view_terms($node, $vid = NULL, $ordered_list = TRUE) {
     
      //Count the number of taxonomy for each node
      $tax_data=$node->taxonomy;     
      $total_tax=count($tax_data);
     
      $vocabularies = taxonomy_get_vocabularies();
      $iv=0;
      $tot_v=count($vocabularies);
       
      //if ($ordered_list) $output .= '<ul>'; //checks to see if you want an ordered list
      if ($vid) { //checks to see if you've passed a number with vid, prints just that vid
        $output = '<div class="tags-'. $vid . '">';
        foreach($vocabularies as $vocabulary) {
         if ($vocabulary->vid == $vid) {
           $terms = taxonomy_node_get_terms_by_vocabulary($node, $vocabulary->vid);
           if ($terms) {
             $links = array();
             $output .= '<span class="only-vocabulary-'. $vocabulary->vid . '">';
             if ($ordered_list) $output .= '<li class="vocabulary-'. $vocabulary->vid . '">';
             
             foreach ($terms as $term) {
               if($vocabulary->name=='Resource Type'){
                    $term_path='resource_by_type/'.$term->tid;
               }
               else if($vocabulary->name=='Primary Subject'){
                    $term_path='resource_by_subject/'.$term->tid;
               }
	       else if($vocabulary->name=='Secondary Subject'){
                    $term_path='resource_by_subject/'.$term->tid;
               }
               else if($vocabulary->name=='Country'){
                    $term_path='resource_by_country/'.$term->tid;
               }
               else if($vocabulary->name=='Events'){
                    $term_path='cea_event_type/'.$term->tid;
               }
		if($iv<$total_tax-1){
		  $links[] = '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description))) .' | </ li>';
		}
		else{
		  $links[] = '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description))) .'</ li>';
		}
		$iv++;             }
	      $output .= implode('  ', $links);
	      if ($ordered_list) $output .= '</li>';
	      $output .= '</span>';
	    }
	  }
	}
      }
      else {
       //$output = '<div class="taxonomy">';
       $term_psubject1=array();
       foreach($vocabularies as $vocabulary) {
         if ($vocabularies) {
           $terms = taxonomy_node_get_terms_by_vocabulary($node, $vocabulary->vid);
           if ($terms) {
             $links = array();
             if ($ordered_list) //$output .= '<li class="vocabulary-'. $vocabulary->vid . '">';
	     
             foreach ($terms as $k=>$term) {
	       
              if($vocabulary->name=='Type'){
                    $term_path='resource_by_type/'.$term->tid;
		    $term_type .= '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description)));
		    if(count($terms)!=$k+1)
		    {
		      $term_type.=' | ';
		    }
                    
		     $term_type.='</li>';	
               }
			   
                if($vocabulary->name=='Subject'){
                    $term_path='resource_by_subject/'.$term->tid;
		    $term_psubject1[] = '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description)));

		     	    
               }
	      /* else if($vocabulary->name=='Secondary Subject'){
                    $term_path='resource_by_subject/'.$term->tid;
		    $term_psubject1[] = '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description)));
    
               }*/
               else if($vocabulary->name=='Country'){
                    $term_path='resource_by_country/'.$term->tid;
		    $term_country .= '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description)));
		    if(count($terms)!=$k+1)
		    {
		      $term_country.=' | ';
		    }
                     
		     $term_country.='</li>';		    
               }
	       /*else if($vocabulary->name=='ECO Asia Event'){
                    $term_path='cea_event_type/'.$term->tid;
		    $term_events .= '<li class="term-' . $term->tid . '">' . l($term->name, $term_path, array('rel' => 'tag', 'title' => strip_tags($term->description)));
		    if(count($terms)!=$k+1)
		    {
		      $term_events.=' | ';
		    }
		     $term_events.='</li>';		    
               } */
             }
	      
             //$output .= implode('  ', $links);
	  }
	  
	}
	
      }

      if(count($term_psubject1)==1){
	$term_psubject=$term_psubject1[0].'</ li>';
      }
      elseif(count($term_psubject1)>1){
	$term_psubject=implode(' | </ li>',$term_psubject1).'</ li>';
      }
      
      
    }
    $terms_link='<div class="term_format_link"><ul>';
      if($term_psubject){
       $terms_link .='<li class="voc_name"></ li>'.$term_psubject.' | ';
      }
      if($term_type){
       $terms_link .=''.substr_replace($term_type,"",-7);
      }
      if($term_country){
       $terms_link .=' | '.substr_replace($term_country,"",-7);
      }
     // if($term_events){
      // $term_event_link='<div class="term_format_link"><ul><li class="voc_name">Event: </ li>'.substr_replace($term_events,"",-7).'</ul><div style="clear:both"></div></div>';
     // }
      
      $output .=$terms_link.'</ul></div>';
      
      return $output;
}
