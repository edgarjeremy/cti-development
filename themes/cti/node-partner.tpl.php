<?php
  $pagePath = $node->path;
  $flag_check = 0;
  if($pagePath == 'events'){
    $flag_check = 1; 
  }
  
?>

<div class="node <?php print $classes; ?>" id="node-<?php print $node->nid; ?>">
  <div class="node-inner">
    <?php if (!$page): ?>
      <h2 class="title"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    
    <div class="content">
      <div class="node_body_data">
      <?php //print $content; ?>
      <?php
        $partner_logo_path = $node->field_partner_logo[0]['filepath'];
        $partner_logo = theme('imagecache', 'partner_logo', $partner_logo_path);
        print $partner_logo;
      ?>
      <?php print $node->content['body']['#value']; ?>
      <br>
      <?php
        $partner_site_url = $node->field_partner_site_url[0]['url'];
        $partner_site = l('Go to Partner Site', $partner_site_url, array('attributes' => array('target' => '_blank')));
        print $partner_site;
      ?>
      </div>
    </div>

    <?php if ($terms): ?>       
         <div class="taxonomy">
           <?php
                //print display_cea_terms($node, $vid = NULL, $unordered_list = true);
           ?>
           <div style="clear:both"></div>
         </div>       
     <?php endif;?>
     <div class="page_links">
          <?php if ($links): ?> 
            <div class="links"> <?php print $links; ?></div>
          <?php endif; ?>
     </div>

  </div> <!-- /node-inner -->
  
  <?php
    if($flag_check != 1){
  ?>
    <div class="print_Ver">        
      <?php print l('<img src="'.base_path().path_to_theme().'/css/images/print_icon.gif
                    " title="Printer-friendly version" alt="Printer-friendly version" />Printer-friendly version', "print/".$node->nid, array('html' => true, 'attributes' => array('target' => '_blank')));?>
    </div>
  <?php
    }
  ?>
  
</div> <!-- /node-->

