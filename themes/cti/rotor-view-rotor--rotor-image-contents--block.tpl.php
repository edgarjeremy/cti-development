<?php
// $Id: rotor-view-rotor.tpl.php,v 1.1.2.6 2009/04/08 22:26:20 mrfelton Exp $
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<script>
    $(document).ready(function() {
        $('#buttons .pause').click(function() {
            
             $('div.rotor-items','#block-views-rotor_image_contents-block_1').cycle('pause');
             $('#buttons .pause').css('display', 'none');
             $('#buttons .play').css('display', 'block');
             
        });
        
        $('#buttons .play').click(function() {
            
             $('div.rotor-items','#block-views-rotor_image_contents-block_1').cycle('resume');
             $('#buttons .play').css('display', 'none');
             $('#buttons .pause').css('display', 'block');
             
             
        });  
    });
</script>
<style type="text/css">
.rotor_button li{
    display: inline;
    float: left;
    list-style: none;
}
</style>
<?php if ($rows): ?>
  <?php $display_tabs = $view->style_plugin->options['tabs']['show_tabs'] && $view->style_plugin->options['tabs']['group_tabs'] == ROTOR_GROUP_TABS; ?>
  <div class='rotor' id="rotor-view-id-<?php print $name; ?>-view-display-id-<?php print $display_id; ?>">
    <?php if ($display_tabs && $options['tabs']['position'] == ROTOR_TAB_POSITION_TOP): ?>
      <?php print theme('rotor_tabs', $view->result); ?>
    <?php endif ?>
    <div class='rotor-items' style="width:<?php print $options['width']; ?>px; height:<?php print $options['height']; ?>px;">
    
        
        
        <?php
            $resultDataRotorImg = $view->result;    
            foreach($resultDataRotorImg AS $resKey => $val){
                $node = node_load($val->nid);
                
                //Here is the changes for the alt and title attribute of image
                $imageAltTag = $node->field_image[0]['data']['alt'];
                if(!empty($imageAltTag)){
                  $alt = $title = $imageAltTag;
                } else {
                  $alt = $title = $val->node_title;
                }
                
                $node->field_image[0]['data']['title'];
                $image_filepath = $node->field_image[0]['filepath'];
                $path='node/'.$val->nid;
                $attributes = '';
                ?>
                
                <div class="rotor-content">            
                    <div class="views-field-title">
                      <span class="field-content"><a title="<?php print $val->node_title; ?>" href="<?php print base_path().drupal_get_path_alias($path);?>"><?php print $val->node_title; ?></a></span>
                    </div>
                    
                    <div class="views-field-field-reports-content-value">
                      <div class="field-content"><?php print $val->node_data_field_reports_content_field_reports_content_value; ?><a title="<?php print $val->node_title; ?>" href="<?php print base_path().drupal_get_path_alias($path);?>">more&#187;</a></div>
                    </div>
                    
                    <div class="views-field-field-image-fid">
                      <span class="field-content">
                        <a title="<?php print $val->node_title; ?>" href="<?php print base_path().drupal_get_path_alias($path);?>"><?php print theme('imagecache', 'rotor_image', $image_filepath, $alt, $title); ?></a>
                      </span>
                    
                    </div>
                    
                </div>
                
                <?php
            }    
        ?>
        
    </div>
    <?php if ($display_tabs && $options['tabs']['position'] == ROTOR_TAB_POSITION_BOTTOM): ?>
      <?php print theme('rotor_tabs', $view->result); ?>
    <?php endif ?>
    
    <div  id ="buttons" class="buttonContainer">
        <div class="buttonContainers">
            <ul class="rotor_button">
            <li><a href="javascript:void();" id="example_prev"><img src="<?php print base_path().path_to_theme();?>/css/images/previous.png" title="previous" alt="Previous"/></a></li>
            <li><a href="javascript:void();" class="pause" style="width:24px;height:26px;"><img id="pause" src="<?php print base_path().path_to_theme();?>/css/images/pause.png" title="pause" alt="Pause"/></a></li>
            <li><a href="javascript:void();" class="play"  style="display:none;width:24px;height:26px;"><img id="play" src="<?php print base_path().path_to_theme();?>/css/images/play.png" title="play" alt="Play"/></a></li>
            <li><a href="javascript:void();" id="example_next"><img src="<?php print base_path().path_to_theme();?>/css/images/next.png" title="next" alt="Next" /></a></li>
            </ul>
            <div style="clear: both"></div>
        </div>
    </div>
    
  </div>
<?php endif; ?>
