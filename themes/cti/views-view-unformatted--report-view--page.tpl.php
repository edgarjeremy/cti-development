<?php
  $report_result=$view->result;
?>
<div class="view-reports-page-data">
  <div class="views-field-title margin-18">
      <h6><?php print "Here is the collection of ECO-Asia Technical Reports. Select a report for more information and download."; ?></h6>
  </div>
  
  <?php
  foreach($report_result as $result_data){
    $node=node_load($result_data->nid);    
    $dateData = $node->field_document_date[0]['value'];
    if(!empty($dateData)){
      $dateValue = strtotime($dateData);
      $reportDate = date("F Y",$dateValue);
    } else {
      $reportDate = "";
    }
  ?>
  <div class="view-reports-page">
    <div class="views-field-field-image-fid">
      <?php
        $field_image=$node->field_image;
        if(!empty($field_image))
        {
          foreach($field_image as $node_image){
            $file_path=$node_image['filepath'];
            $img_alt=$node->title;
            
            //Here is the changes for the alt and title attribute of image
            $imageAltTag = $node->field_image[0]['data']['alt'];
            if(!empty($imageAltTag)){
              $alt = $title = $imageAltTag;
            } else {
              $alt = $title = $val->node_title;
            }
                
            if(!empty($node_image['fid'])){
              print '<span class="field-content">'. '<a href="' .base_path().$node->path. '" title="'.$img_alt.'">' . theme('imagecache','landing_page_thumbnail',$file_path,$alt, $title).'</a></span>';
            }
          }
        }        
      ?>
      
    </div>
    
    <div class="views-field-title">
      <span class="field-content">
        <a alt="<?php print $node->title;?>" title="<?php print $node->title;?>" href="<?php print base_path().$node->path;?>">
          <?php print $node->title;?>
        </a>
      </span>
    </div>
    
    <?php if($reportDate != ""){ ?>
      <div class="dateContent">
        <span class="field-content">
          <?php print $reportDate;?>
        </span>
      </div>
    <?php } ?>
    <div class="views-field-body">
      <span class="field-content">
        <?php print _substr(strip_tags($node->field_reports_content[0]['value']), 370,3);?>
        <a class="cea_more_links" alt="<?php print $node->title;?>" title="<?php print $node->title;?>" href="<?php print base_path().$node->path;?>">
          more&raquo;
        </a>
      </span>
    </div>
    
    
  </div>
  <?
  }
  ?>  
</div>
