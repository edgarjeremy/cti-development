<?php
// $Id: views-view-unformatted.tpl.php,v 1.6 2008/10/01 20:52:11 merlinofchaos Exp $
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
   

   $results=$view->result;
 /*    print'<pre>';
    print_r($results);
    print'</pre>';*/
?>

    <div id="view_all_events_main">
        
        <?php
        
        foreach($results as $val ){
                $my_date = strtotime($val->node_data_field_start_date_field_start_date_value);
                $event_end_date=strtotime($val->node_data_field_start_date_field_end_date_value);
                $event_month=date("M",$my_date);
                $event_day=date("d",$my_date);
                $start_date =  date("j F Y",$my_date);
                if($event_end_date){
                    $end_date = " - ".date("j F Y",$event_end_date);
                }
                
                $text=$val->node_title;
                $path='node/'.$val->nid;
        ?>
        
        <div id="view_all_events_inner">
                <!--div class="<?php print $classes[$id]; ?>"-->
                <div class="views-row views-row-2 views-row-even">
                        <div class="view_all_events_header">
                                <div class="event_month">
                                        <?php print  $event_day;?>
                                </div>
                                <div class="event_day">
                                        <?php print $event_month; ?>
                                </div>
                        
                        </div>
                        
                        <div class="views-field-title">                              
                                <span class="field-content">
                                 <a title="<?php print $val->node_title; ?>" href="<?php print drupal_get_path_alias($path);?>"><?php print $val->node_title; ?></a>
                                </span>
                        </div>
                        
                        <div class="views-field-field-start-date-value">
                                <span class="field-content"><span class="date-display-single"><?php echo $start_date.$end_date; ?></span></span>
                        </div>
                        
                        <div class="views-field-body">
                                 <span class="field-content"><?php echo strip_tags(_substr($val->node_revisions_body,240,3));?> <a class="event_more_link" title="<?php print $val->node_title; ?>" href="<?php print drupal_get_path_alias($path); ?>">more&raquo;</a></span>
                        </div>
                </div>                
        </div>
        <?php
                }
        ?>
</div>
