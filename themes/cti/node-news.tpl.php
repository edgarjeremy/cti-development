<?php
    $bodyValue = $node->content['body']['#value'];
    $imagePath1 = trim($node->field_image[0]['filepath']);
    $imagePath2 = trim($node->field_image[1]['filepath']);
    $imagePath3 = trim($node->field_image[2]['filepath']);
    $attachmentValue = $node->field_news_attachment;
   // $shareth = module_invoke('block', 'block', 'view', 21);
    
    $my_date = strtotime($node->field_document_date[0]['value']);
    $new_day = date("d",$my_date);
    $news_date =  $new_day." ".date("F Y",$my_date);
    
    $node_files=$attachmentValue;
    
    $fileNameValue = $node_files[0]['filename'];
    
    //Here is the changes for the alt and title attribute of image
    $imageAltTag = $node->field_image[0]['data']['alt'];
    if(!empty($imageAltTag)){
      $alt = $titleValue = $imageAltTag;
    } else {
      $alt = $titleValue = $node->title;
    }
    
      foreach($node_files as $file){
        $filedescription=$file['description'];
        $filedate=date('F Y',$file['timestamp']);
        $filepath=$file['filepath'];
        
        $file_type=explode('.',$filepath);
        $filetype_disp=strtolower($file_type[count($file_type)-1]);
        
        if($filetype_disp=='pdf'){
          $img_disp='<img src="'.base_path().path_to_theme().'/css/doc_types/icon_'.$filetype_disp.'.gif" alt="'.$alt.'" title="'.$titleValue.'">';
        }
        else if($filetype_disp=='doc' || $filetype_disp=='docx'){
          $change_type='doc';
          $img_disp='<img src="'.base_path().path_to_theme().'/css/doc_types/icon_'.$change_type.'.gif" alt="'.$alt.'" title="'.$titleValue.'">';
        }
        else {
          $change_type='generic';
          $img_disp='<img src="'.base_path().path_to_theme().'/css/doc_types/icon_'.$change_type.'.gif" alt="'.$alt.'" title="'.$titleValue.'">';
        }
        
        $file_path_for_download = $file["filepath"];
                
        if(!(stripos($file_path_for_download,'&'))){
          $file_download_path=base_path().drupal_get_path('module', 'pubdlcnt') .'/pubdlcnt.php?file='.base_path().$file_path_for_download.'&nid='.$node->nid;
        } else {
          $file_download_path = base_path().$file_path_for_download;
        }
        
        $file_url .= '<li>'.$img_disp.' <a href="'.$file_download_path.'" title="'.$title.'"><b>Download File</b></a></li>';
                
                
        //$file_url .= '<li>'.$img_disp.' <a href="'.base_path().drupal_get_path("module", "pubdlcnt") .'/pubdlcnt.php?file='.base_path().$file["filepath"].'&nid='.$node->nid.'"><b>Download File</b></a></li>';
      }
?>

<div class="node <?php print $classes; ?>" id="node-<?php print $node->nid; ?>">
    <div class="node-inner">
        <div id="rlLanding_common_container">            
          <div class="rl_commonExeptWidth rl_eachContents">
            <div class="share-this-block"> 
                <span class='title'>Share this on: </span>
                <span class='share_facebook'></span>
                <span class='share_twitter'></span>
            </div>
              <?php if($imagePath1 != ''){ ?>
                <div class="rl_commonExeptWidth rl_leftImage">
                    <?php print theme('imagecache','node_page_image',$imagePath1, $alt, $titleValue); ?>
                    <?php print '<div class="image_caption">' . $node->field_photo_caption_1[0]['value'] . '</div>'; ?>
                    <?php if($imagePath2 != ''){ ?>
                    <?php print theme('imagecache','node_page_image',$imagePath2, $alt, $titleValue); ?>
                    <?php print '<div class="image_caption">' . $node->field_photo_caption_2[0]['value'] . '</div>'; ?>
                    <?php } ?>
                    <?php if($imagePath3 != ''){ ?>
                        <?php print theme('imagecache','node_page_image',$imagePath3, $alt, $titleValue); ?>
                        <?php print '<div class="image_caption">' . $node->field_photo_caption_3[0]['value'] . '</div>'; ?>
                    <?php } ?>
                </div><!--End of rl_leftImage class -->
              <?php } ?>
              
              <div class="rl_rightContainer">
               
                <div class="rl_taxonomy">
                  <div class=news-page-content>
                  <?php print $bodyValue; ?>
                  </div>
                </div><!--End of rl_taxonomy class -->                
                
                    <?php
                     if(!empty($fileNameValue)){
                        echo '<ul class="downloadClass">'.$file_url.'</ul>';
                     }
                    ?>
                <div class="news_date">Date:&nbsp;<?php print $news_date; ?></div>    
              </div> <!--End of rl_rightContainer class -->              
              
              <?php if ($terms): ?>
                <div class="rl_commonExeptWidth rl_texonomyCont">
                  <div class="taxonomy">
                    <?php                     
                        print display_cea_terms($node, $vid = NULL, $unordered_list = true);
                     ?>
                  </div>
                </div>
              <?php endif;?>
              
              
              
          </div><!--End of rl_eachContents class -->
        </div><!--End of the div rl_common_container -->
        
            <div class="node_navigate_links">
              <?php print l('News Section','news',array('attributes'=>array('title'=>'News Section'))); ?> |
                <a href="<?php print base_path();?>" title="Site home">Home</a>
            </div>
            
  </div> <!-- /node-inner -->
  
  <div class="print_Ver">        
    <?php print l('<img src="'.base_path().path_to_theme().'/css/images/print_icon.gif
                  " title="Printer-friendly version" alt="Printer-friendly version" />Printer-friendly version', "print/".$node->nid, array('html' => true, 'attributes' => array('target' => '_blank')));?>
  </div>
  
</div> <!-- /node-->