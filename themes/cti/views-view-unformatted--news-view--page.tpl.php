<?php
$report_result=$view->result;
?>
<div class="view-reports-page-data">
    
  <?php
  foreach($report_result as $result_data){
  $node=node_load($result_data->nid);
  $my_date = strtotime($node->field_document_date[0]['value']);
  //$new_day = date("j",$my_date) + 1;
  $new_day = date("j",$my_date);
  $news_date =  $new_day." ".date("F Y",$my_date);
  ?>
  <div class="view-reports-page">
    <div class="views-field-title">
      <span class="field-content">
        <a alt="$node->$title" title="<?php print $node->title;?>" href="<?php print base_path().$node->path;?>">
          <?php print $node->title;?>
        </a>
      </span>
    </div>
    <div class="views-field-field-start-date-value">
        <span class="field-content"><span class="date-display-single"><?php echo $news_date?></span></span>
    </div>
                        
    <div class="views-field-body">
      <span class="field-content">
        <?php print _substr(strip_tags($node->body), 200);?>
        <a class="cea_more_links" title="<?php print $node->title;?>" href="<?php print base_path().$node->path;?>">
          more&raquo;
        </a>
      </span>
    </div>
    
    <div style="clear: both"></div>
  </div>
  <?php
  }
  ?>  
</div>
