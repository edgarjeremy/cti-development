<div class="view view-<?php print $css_name; ?> view-id-<?php print $name; ?> view-display-id-<?php print $display_id; ?> view-dom-id-<?php print $dom_id; ?>">
  <?php if ($admin_links): ?>
    <div class="views-admin-links views-hide">
      <?php print $admin_links; ?>
    </div>
  <?php endif; ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>
  
  <?php if ($exposed): ?>
    <div class="view-filters">
      <div class="titleOfExposeView" id="titleOfExposeView<?php print $display_id;?>">Choose Criteria</div>
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
    <div class="view-content">
        
        <div id="rl_common_container"> <!--Start of rl_common_container class -->
            <?php
                $results = $view->result;
                $fileName= '';
                foreach($results AS $resKey => $val){
                    $node = node_load($val->nid);
                    $imageVal = $node->files;
                    $fileName = '';
                    foreach($imageVal AS $fileVal){
                      if(($fileVal->filename) != '')  $fileName .= $fileVal->filename;                      
                    }
                    
                    //Get the image name from file extension
                    $imageName = getImageNameWithFileExtenstion($fileName);
                    
                    //Here is the changes for the alt and title attribute of image
                    $imageAltTag = $node->field_image[0]['data']['alt'];
                    if(!empty($imageAltTag)){
                      $alt = $title = $imageAltTag;
                    } else {
                      $alt = $title = $node->title;;
                    }
                    
                    $authorName = $node->field_institutional_author[0]['value'];
                    $dateValue = $node->field_document_date[0]['value'];
                    $get_date=strtotime($dateValue);
  
                    ?>
                    
                    <div class="rl_commonExeptWidth rl_eachContents">
                        <div class="rl_commonExeptWidth rl_leftImage">
                            <a href="<?php print base_path().$node->path;?>" title="<?php print $title;?>" ><img alt="<?php print $alt;?>" src="<?php print base_path().path_to_theme();?>/css/images/<?php echo $imageName;?>.png" /></a>
                        </div><!--End of rl_leftImage class -->
                        
                        <div class="rl_commonExeptWidth rl_rightContainer">
                            <div class="rl_title">                    
                                <?php         
                                $titleRl = ucfirst($node->title);
                                echo $titleValue = l($titleRl,$node->path);
                                ?>
                            </div><!--End of rl_title class -->
                            
                            <div class="content_file_links">
                              <?php
                                $field_file_location_url = $node->field_file_location_url[0][url];
                                $file_location_url = "<a href='$field_file_location_url'>Download file from source</a>";
                                   
                                $files = $node->files;
                                $file_url = '';
                                foreach($files as $file){
                                    $file_path_for_download = $file->filepath;
                                    $filepath=$file->filepath;
                                    $file_type=explode('.',$filepath);
                                    $filetype_disp=strtolower($file_type[count($file_type)-1]);
                                    
                                    if($filetype_disp=='pdf'){
                                      $img_disp='<img src="'.base_path().path_to_theme().'/css/doc_types/icon_'.$filetype_disp.'.gif" title="'.$title.'" alt="document_'.$filetype_disp.'">';
                                    }
                                    else if($filetype_disp=='doc' || $filetype_disp=='docx'){
                                      $change_type='doc';
                                      $img_disp='<img src="'.base_path().path_to_theme().'/css/doc_types/icon_'.$change_type.'.gif" title="'.$title.'" alt="document_'.$filetype_disp.'">';
                                    }
                                    else {
                                      $change_type='generic';
                                      $img_disp='<img src="'.base_path().path_to_theme().'/css/doc_types/icon_'.$change_type.'.gif" title="'.$title.'" alt="document_'.$filetype_disp.'">';
                                    }
                                    
                                    if(!(stripos($file_path_for_download,'&'))){
                                      $file_download_path = base_path().drupal_get_path('module', 'pubdlcnt') .'/pubdlcnt.php?file='.base_path().$file_path_for_download.'&nid='.$node->nid;
                                    } else {
                                      $file_download_path = base_path().$file_path_for_download;
                                    }
                                    
                                    $file_url .= $img_disp.' <a href="'.$file_download_path.'" title="'.$title.'"><b>Download File</b></a>';
                                }
                                
                                $readmore = "<a href='".base_path()."$node->path'>Read More</a>";
                                echo "<ul>";
                                echo "<li>$readmore</li>";                                 
                                if($field_file_location_url != '')
                                  //echo "<li>$file_location_url</li>";
                                  echo "</ul>";
                                 
                              ?>
                              
                            </div>
                                  
                            <?php if(count($files)>0){ ?>
                              <div class="contentDetail authorView">
                                  <?php print $file_url;?>        
                              </div>
                            <?php } ?>
                            
                            <?php if(!empty($authorName)){ ?>
                              <div class="contentDetail authorView">
                                  <b>Author</b> : 
                                  <?php print $authorName;?>        
                              </div>
                            <?php } ?>
                            
                            <?php if(!empty($dateValue)){ ?>
                              <div class="contentDetail authorView">
                                  <b>Date</b> : 
                                  <?php echo date('d-m-Y',$get_date);?>        
                              </div>
                            <?php } ?>
                            
                            <?php if ($node->taxonomy): ?>
                              <div class="rl_commonExeptWidth rl_texonomyCont">
                                <div class="taxonomy">
                                  <?php
                                       print display_cea_terms($node, $vid = NULL, $unordered_list = true);
                                  ?>
                                </div>
                              </div>
                            <?php endif;?><!--End of rl_taxonomy class -->
                            
                            
                        </div> <!--End of rl_rightContainer class -->
                        
                        
                    </div><!--End of rl_eachContents class -->
                    
                    <?php
                }
                
            ?>
        </div><!--End of rl_common_container class -->
        
        
        
      <?php //print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div> <?php /* class view */ ?>
