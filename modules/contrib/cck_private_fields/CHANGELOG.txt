; $Id: CHANGELOG.txt,v 1.1.2.18 2009/08/01 20:19:41 markuspetrux Exp $

6.x-1.x-beta3
=============

Bug fixes:
- Only users allowed to edit privacy settings should be allowed to do so.


6.x-1.x-beta2
=============

New features:
- #536746 Added support for fields in multigroups. Requires update.php.


6.x-1.x-beta1
=============

- Fix {cck_private_fields} table definition (requires update.php).
- Externalized most of the code into separate includes that are loaded on demand
  to reduce memory consumption during normal site operation.
- Yet another revision of the {cck_private_fields} table. Requires update.php.
- #520226 - Support for node revisions is now completed.
- Integration with diff module has been rewritten to provide more user friendly information.
  Also, changes to privacy status is only visible to node author or users with 'view private cck fields' permission.
- Management of the node edit form has been rewritten and simplified.
- Fixed bug when checking for 'view private cck fields' permission in hook_field_access().
- Fixed bug in cck_private_fields_get_content_private_fields().
- #531562 Field privacy option in {variable} table should not include the content type in the key. Requires update.php.
- #531892 Delete privacy data when privacy option is disabled.
- #531976 Create system permissions for private fields per field, not global.
- #531900 Changed integration API.
- #531900 Integration with FriendList API.
- #531900 Integration with User Relationships module.
- Enhanced documentation for developers in the DEVELOPMENT / API section of the module README.
- Fix function not found when a CCK fields is deleted.
- #531896 Provide a javascript based widget to change field privacy options in node edit form. Requires jQuery UI module!
