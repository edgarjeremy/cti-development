// $Id: kml.js,v 1.1.2.6 2010/03/22 23:16:49 zzolo Exp $

/**
 * @file
 * Layer handler for KML layers
 */

/**
 * Openlayer layer handler for KML layer
 */
Drupal.openlayers.layer.kml = function(title, map, options) {
  // Get styles
  var styleMap = Drupal.openlayers.getStyleMap(map, options.drupalID);
  
  // Format options
  if (options.maxExtent !== undefined) {
    options.maxExtent = OpenLayers.Bounds.fromArray(options.maxExtent);
  }

  options.format = OpenLayers.Format.KML;
  options.projection = new OpenLayers.Projection('EPSG:4326');
  
  // Create layer
  var layer = new OpenLayers.Layer.GML(title, options.url, options);
  layer.styleMap = styleMap;
  return layer;
};
