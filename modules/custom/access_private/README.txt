/* $Id: README.txt,v 1.1 2010/08/13 17:32:21 stborchert Exp $ */

"Access Private" allows you to access private nodes by using a special link 
containing their Universal Unique IDentifier (UUID).

This makes it possible to share private nodes with people who know this link.
