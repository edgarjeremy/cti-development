<?php
/**
 * @file
 * Contains the default style plugin.
 */

/**
 * Default style plugin to render rows one after another with no
 * decorations.
 *
 * @ingroup views_style_plugins
 */
class view_news_plugin_style extends views_plugin_style {
  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['type'] = array('default' => 'ul');
    $options['filter_year'] = array('default' => '');
    $options['field_year'] = array('default' => '');

    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['type'] = array(
      '#type' => 'radios',
      '#title' => t('List type'),
      '#options' => array('ul' => t('Unordered list'), 'ol' => t('Ordered list')),
      '#default_value' => $this->options['type'],
    );
    
    $form['filter_year'] = array(
      '#type' => 'textfield',
      '#title' => t('From Year Filter'),
      '#default_value' => $this->options['filter_year'],
    );
    $form['field_year'] = array(
      '#type' => 'textfield',
      '#title' => t('Field Year'),
      '#default_value' => $this->options['field_year'],
    );
  }
}
