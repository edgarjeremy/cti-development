<?php

/**
 * Implementation of hook_views_plugins
 */
function view_news_views_plugins() {
  $path = drupal_get_path('module', 'view_news');
  $views_path = drupal_get_path('module', 'views');
  require_once "./$path/theme/theme.inc";
  return array(
    'module' => 'view_news',
    'style' => array(
      'view_news_style' => array(
        'title' => t('CTICFF News View'),
        'help' => t('Displays nodes.'),
        'handler' => 'view_news_plugin_style',
        'path' => "$path/views",
        'theme' => 'view_news_view_style',
        'theme file' => 'theme.inc',
        'theme path' => "$path/theme",
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
    'row' => array(
      'view_news_row' => array(
        'title' => t('CTICFF News Row'),
        'help' => t('(Displays a CTICFF Format Row'),
        'handler' => 'view_news_plugin_row',
        'path' => "$path/views",
        'theme' => 'view_news_view_row',
        'theme file' => 'theme.inc',
        'theme path' => "$path/theme",
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
    )
  );
}
