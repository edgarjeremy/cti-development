<?php 
// $Id$
/**
 * @file
 */

function node_list($type) {
  $result = array();
  $sql = "SELECT * FROM {node} WHERE status=1 AND type='%s' ORDER BY nid DESC";
  $query = db_query(db_rewrite_sql($sql), $type);
  
  while ($data = db_fetch_object($query)) {
    $result[] = array(
      'nid' => $data->nid,
      'title' => $data->title
    );
  }
  
  echo json_encode($result);
}

function node_type_list() {
  $result = array();
  $sql = "SELECT type, name FROM {node_type} ORDER BY name ASC";
  $query = db_query(db_rewrite_sql($sql));
  
  while ($data = db_fetch_object($query)) {
    $result[] = array(
      'type' => $data->type,
      'name' => $data->name
    );
  }
  
  echo json_encode($result);
}
