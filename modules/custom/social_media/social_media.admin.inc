<?php 
// $Id$
/**
 * @file
 * Administration page callbacks for the social_media module.
 */

/**
 * Form builder.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function social_media_admin_settings() {
  $form['social_media'] = array(
    '#type' => 'fieldset',
    '#title' => t('Social Media'),
  );
  $form['social_media_twitter'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter'),
    '#default_value' => variable_get('social_media_twitter', ''),
  );
  $form['social_media_facebook'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook'),
    '#default_value' => variable_get('social_media_facebook', ''),
  );
  $form['social_media_linkedin'] = array(
    '#type' => 'textfield',
    '#title' => t('Linkedin'),
    '#default_value' => variable_get('social_media_linkedin', ''),
  );
  $form['social_media_instagram'] = array(
    '#type' => 'textfield',
    '#title' => t('Instagram'),
    '#default_value' => variable_get('social_media_instagram', ''),
  );
  $form['social_media_youtube'] = array(
    '#type' => 'textfield',
    '#title' => t('Youtube'),
    '#default_value' => variable_get('social_media_youtube', ''),
  );

  return system_settings_form($form);
}
