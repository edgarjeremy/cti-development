<?php

/**
 * @file theme.inc
 * Preprocessing for views themes.
 */

/**
 * Display the simple view of rows one after another
 */
function template_preprocess_view_collection_view_style(&$vars) {
  $view     = $vars['view'];
  $rows     = $vars['rows'];

  $vars['query'] = $view->build_info['query'];
  $vars['query_args'] = $view->build_info['query_args'];
  $vars['items_per_page'] = $view->pager['items_per_page'];
  $vars['view_name'] = $view->name;
  $vars['title'] = $view->display_handler->default_display->options['title'];
  $vars['classes'] = array();
}

/**
 * Display the simple view of rows one after another
 */
function template_preprocess_view_collection_view_row(&$vars) {
  $view = $vars['view'];

  // Loop through the fields for this view.
  $inline = FALSE;
  $vars['fields'] = array(); // ensure it's at least an empty array.
  foreach ($view->field as $id => $field) {
    // render this even if set to exclude so it can be used elsewhere.
    $field_output = $view->style_plugin->get_field($view->row_index, $id);
    $empty = $field_output !== 0 && empty($field_output);
    if (empty($field->options['exclude']) && (!$empty || (empty($field->options['hide_empty']) && empty($vars['options']['hide_empty'])))) {
      $object = new stdClass();

      $object->content = $field_output;
      if (isset($view->field[$id]->field_alias) && isset($vars['row']->{$view->field[$id]->field_alias})) {
        $object->raw = $vars['row']->{$view->field[$id]->field_alias};
      }
      else {
        $object->raw = NULL; // make sure it exists to reduce NOTICE
      }
      $object->inline = !empty($vars['options']['inline'][$id]);
      $object->inline_html = $object->inline ? 'span' : 'div';
      if (!empty($vars['options']['separator']) && $inline && $object->inline && $object->content) {
        $object->separator = filter_xss_admin($vars['options']['separator']);
      }

      $inline = $object->inline;

      $object->handler = &$view->field[$id];
      $object->element_type = $object->handler->element_type();

      $object->class = views_css_safe($id);
      $object->label = check_plain($view->field[$id]->label());
      $vars['fields'][$id] = $object;
    }
  }
}
