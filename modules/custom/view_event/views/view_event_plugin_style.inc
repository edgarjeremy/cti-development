<?php
/**
 * @file
 * Contains the default style plugin.
 */

/**
 * Default style plugin to render rows one after another with no
 * decorations.
 *
 * @ingroup views_style_plugins
 */
class view_event_plugin_style extends views_plugin_style {
  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();
    
    $options['filter_year'] = array('default' => '');
    $options['old_year'] = array('default' => '2012');
    $options['filter_month'] = array('default' => '');
    $options['filter_taxonomy'] = array('default' => '');
    $options['start_date'] = array('default' => '');
    $options['end_date'] = array('default' => '');
    $options['title'] = array('default' => '');
    $options['taxonomy_column'] = array('default' => '');
    $options['event_status'] = array('default' => '');

    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['start_date'] = array(
      '#type' => 'textfield',
      '#title' => t('Field start date'),
      '#default_value' => $this->options['start_date'],
    );
    $form['end_date'] = array(
      '#type' => 'textfield',
      '#title' => t('Field end date'),
      '#default_value' => $this->options['end_date'],
    );
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Field title'),
      '#default_value' => $this->options['title'],
    );
    
    $form['filter_year'] = array(
      '#type' => 'textfield',
      '#title' => t('Filter Year'),
      '#default_value' => $this->options['filter_year'],
    );
    $form['old_year'] = array(
      '#type' => 'textfield',
      '#title' => t('Start Year'),
      '#default_value' => $this->options['old_year'],
    );
    $form['filter_month'] = array(
      '#type' => 'textfield',
      '#title' => t('Filter Month'),
      '#default_value' => $this->options['filter_month'],
    );
    $form['filter_taxonomy'] = array(
      '#type' => 'textfield',
      '#title' => t('Filter Category'),
      '#default_value' => $this->options['filter_taxonomy'],
    );
    $form['taxonomy_column'] = array(
      '#type' => 'textfield',
      '#title' => t('Taxonomy Column'),
      '#default_value' => $this->options['taxonomy_column'],
    );
    $form['event_status'] = array(
      '#type' => 'textfield',
      '#title' => t('Event Status'),
      '#default_value' => $this->options['event_status'],
    );
  }
  
  /**
   * Render the display in this style.
   */
  function render() {
    if ($this->uses_row_plugin() && empty($this->row_plugin)) {
      vpr('views_plugin_style_default: Missing row plugin');
      return;
    }

    // Group the rows according to the grouping field, if specified.
    $sets = $this->render_grouping($this->view->result, $this->options['grouping']);

    // Render each group separately and concatenate.  Plugins may override this
    // method if they wish some other way of handling grouping.
    $output = '';
    foreach ($sets as $title => $records) {
      $rows = array();
      foreach ($records as $row_index => $row) {
        $this->view->row_index = $row_index;
        $index_date = date('Y', strtotime($row->{$this->options['filter_month']}));
        $rows[$index_date][] = $this->row_plugin->render($row);
      }
      
      krsort($rows);
      
      $output .= theme($this->theme_functions(), $this->view, $this->options, $rows, $title);
    }
    unset($this->view->row_index);
    return $output;
  }
}
