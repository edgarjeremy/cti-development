<div class='row wrap-data'>
  <?php foreach ($fields as $id => $field): ?>
    <<?php print $field->inline_html;?> class="views-field-<?php print $field->class; ?>" <?php print ($field->class == 'field-event-status-value') ? 'hidden' : 'show' ; ?>>
      <<?php print $field->element_type; ?> class=""><?php print $field->content; ?></<?php print $field->element_type; ?>>
    </<?php print $field->inline_html;?>>
  <?php endforeach; ?>
</div>