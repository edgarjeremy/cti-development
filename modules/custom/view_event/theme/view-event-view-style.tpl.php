<?php
/**
 * @file views-view-list.tpl.php
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<?php 
  global $base_url;
  global $base_path;
  
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '');
  $breadcrumb[] = l(drupal_get_title(), $base_url . base_path() . $view->display_handler->options['path']);

  // Set Breadcrumbs
  $breadcrumbs = drupal_set_breadcrumb($breadcrumb);
?>
<section class='event-page'>
  <div class='entry-header'>
    <div class='content'>
      <div class="breadcrumb">
        <ol class="breadcrumb">
          <?php foreach($breadcrumbs as $breadcrumb): ?>
            <li><?php echo $breadcrumb; ?></li>
          <?php endforeach; ?>
        </ol>
      </div>
      <?php if (!empty($title)) : ?>
        <h1><?php print $title; ?></h1>
      <?php else: ?>
        <h1>Event Page</h1>
      <?php endif; ?>
      <!-- <div class='filter-event'>
        <form class='form-inline' name="formFilter" id="form-filter">
          <div class='form-group'>
             <label>
              VIEWS :
            </label>
          </div>
          <div class='form-group'>
            <select name="<?php print $options['filter_year']; ?>" id='year'>
              <?php for($i = date('Y'); $i >= 2012; $i--): ?>
                <option value='<?php echo $i; ?>'><?php echo $i; ?></option>
              <?php endfor;?>
            </select>
          </div>
          <div class='form-group'>
            <select name="<?php print $options['filter_month']; ?>" id='month'>
              <option >Select Month</option>
              <option value=''>All Month</option>
              <?php for($i = 1; $i <= 12; $i++): ?>
                <?php $date = "2010-$i-10"; ?>
                <option value='<?php echo date("m", strtotime($date)); ?>' <?php if(date("n") == $i): ?>selected="selected"<?php endif; ?>><?php echo date("F", strtotime($date)); ?></option>
              <?php endfor;?>
            </select>
          </div>
          <div class='form-group'>
            <?php
            $sql = "SELECT * FROM {vocabulary_node_types} v
                    LEFT JOIN {term_data} term ON v.vid=term.vid
                    WHERE type = '".$options['filter_taxonomy']."'";
            $query_dd = db_query($sql);
            ?>
            <select name="<?php print $options['taxonomy_column']; ?>" id='category'>
              <option value=''>Select Location</option>
              <option value=''>All Country</option>
              <?php  while ($data = db_fetch_object($query_dd)): ?>
                <option value='<?php print $data->tid; ?>'><?php print $data->name; ?></option>
              <?php endwhile;?>
            </select>
          </div>
          <div class='form-group'>
            <input name='submit' id='submitFilter' type='submit' value='GO'>
          </div>
        </form>
      </div> -->
    </div>
  </div>
  <div class='entry-content' id='entry-content'>
    <div class='entry-nav entry-nav-event-page'>
      <div class='owl-nav' id='customNavEventPage'></div>
      <div class='owl-dots' id='customDotsEventPage'></div>
    </div>
    <div class='owl-carousel owl-theme' id='event-page-feature'>
      <?php foreach ($rows as $row_date => $list_row): ?>
        <div class='slide'>
          <div class='row'>
            <div class='col-md-2'>
              <span class='month'>
              Year
                <?php //echo date("F", strtotime($row_date."-01")); ?>
              </span>
              <span class='year'>
                <?php echo date("Y", strtotime($row_date."-01")); ?>
              </span>
            </div>
            <div class='col-md-10'>
              <div class='row-event'>
                <?php foreach ($list_row as $id => $row): ?>
                  <?php print $row; ?>
                <?php endforeach; ?>
              </div>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>

<script type="text/javascript">
  $(document).ready(function(){

    target = $('#form-filter').closest('.view')[0];
    
    $('#form-filter').submit(function(){
      var yearName = $('#year').attr("name");
      yearValue = $('#year').val();
      
      var monthName = $('#month').attr("name");
      monthValue = $('#month').val();
      
      var catName = $('#category').attr("name");
      catValue = $('#category').val();
      
      view_form = $('.page-content').clone();
      view_form.find('.entry-content').remove();
      
      var post = {
        "args": <?php echo json_encode($query_args); ?>,
        "filters": [{ 
          "name": yearName, "type": "range", 
          "value1":yearValue, 
          "value2":yearValue
          // "value1":yearValue + '-' + monthValue + '-01', 
          // "value2":yearValue + '-' + monthValue + '-30'
          {
            "name": catName, 
            "type":"none", 
            "value": catValue
            }],
        "show_per_page": "<?php echo $items_per_page; ?>",
        "query": "<?php echo trim(preg_replace('/\s\s+/', ' ', $query)); ?>",
        "view_name": "<?php echo $view_name; ?>",
        "view_form": view_form.html(),
      };
      
      $.post("<?php echo $base_url; ?>/api/v1/event/list/1", post, function(response, textStatus,jqXHR){
        $(this).removeClass('views-throbbing');
        // Scroll to the top of the view. This will allow users
        // to browse newly loaded content after e.g. clicking a pager
        // link.
        var offset = $(target).offset();
        // We can't guarantee that the scrollable object should be
        // the body, as the view could be embedded in something
        // more complex such as a modal popup. Recurse up the DOM
        // and scroll the first element that has a non-zero top.
        var scrollTarget = target;
        while ($(scrollTarget).scrollTop() == 0 && $(scrollTarget).parent()) {
          scrollTarget = $(scrollTarget).parent()
        }
        // Only scroll upward
        if (offset.top - 10 < $(scrollTarget).scrollTop()) {
          $(scrollTarget).animate({scrollTop: (offset.top - 10)}, 500);
        }
        // Call all callbacks.
        if (response.__callbacks) {
          $.each(response.__callbacks, function(i, callback) {
            eval(callback)(target, response);
          });
        }
        
        $('#year').val(yearValue);
        $('#month').val(monthValue);
        $('#category').val(catValue);
        new window.CoralCTI.Frontend({});
      }, 'json');
      
      return false;
    });
  });
  // window.onload = function(){
  //     target = $('#form-filter').closest('.view')[0];
  //     var d = new Date();
  //     var n = d.getMonth()+1;
  //     var monthNames = ["January", "February", "March", "April", "May","June","July", "August", "September", "October", "November", "December"];

  //     var yearName = $('#year').attr("name");
  //     yearValue = $('#year').val();

  //     var monthName = $('#month').attr("name");
  //     monthValue = $('#month').val();

  //     var catName = $('#category').attr("name");
  //     catValue = $('#category').val();

  //     view_form = $('.page-content').clone();
  //     view_form.find('.entry-content').remove();

  //     var post = {
  //       "args": <?php echo json_encode($query_args); ?>,
  //       "filters": [{ "name": yearName, "type": "range", "value1":yearValue + '-' + monthValue + '-01', "value2":yearValue + '-' + monthValue + '-30' }, {"name": catName, "type":"none", "value": catValue}],
  //       "show_per_page": "<?php echo $items_per_page; ?>",
  //       "query": "<?php echo trim(preg_replace('/\s\s+/', ' ', $query)); ?>",
  //       "view_name": "<?php echo $view_name; ?>",
  //       "view_form": view_form.html(),
  //     };

  //     $.post("<?php echo $base_url; ?>/api/v1/event/list/1", post, function(response, textStatus,jqXHR){
  //       $(this).removeClass('views-throbbing');
  //       var offset = $(target).offset();
  //       var scrollTarget = target;
  //       while ($(scrollTarget).scrollTop() == 0 && $(scrollTarget).parent()) {
  //         scrollTarget = $(scrollTarget).parent()
  //       }
  //       if (offset.top - 10 < $(scrollTarget).scrollTop()) {
  //         $(scrollTarget).animate({scrollTop: (offset.top - 10)}, 500);
  //       }
  //       if (response.__callbacks) {
  //         $.each(response.__callbacks, function(i, callback) {
  //           eval(callback)(target, response);
  //         });
  //       }
  //       $('#year').val(yearValue);
  //       $('#month').val(monthValue);
  //       $('#category').val(catValue);
  //       new window.CoralCTI.Frontend({});
  //     }, 'json');

  //     document.getElementById("entry-content").style.display = "block";
  // };
  window.onload = function(){
    // var btnSubmitTags = document.getElementById('submitFilter');
    // btnSubmitTags.click();
  };
</script>