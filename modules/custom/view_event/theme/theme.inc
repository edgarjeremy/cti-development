<?php

/**
 * @file theme.inc
 * Preprocessing for views themes.
 */

/**
 * Display the simple view of rows one after another
 */
function template_preprocess_view_event_view_style(&$vars) {
  $view     = $vars['view'];

  $vars['query'] = $view->build_info['query'];
  $vars['query_args'] = $view->build_info['query_args'];
  $vars['items_per_page'] = $view->pager['items_per_page'];
  $vars['view_name'] = $view->name;
  $vars['title'] = $view->display_handler->options['title'];
  $vars['classes'] = array();
}

/**
 * Display the simple view of rows one after another
 */
function template_preprocess_view_event_view_row(&$vars) {
  $view = $vars['view'];
  $options = $view->style_options;

  // Loop through the fields for this view.
  $inline = FALSE;
  $vars['fields'] = array(); // ensure it's at least an empty array.
  $start_date = '';
  $end_date = '';
  $title = '';
  $event_status = '';
  
  foreach ($view->field as $id => $field) {
    // render this even if set to exclude so it can be used elsewhere.
    $field_output = $view->style_plugin->get_field($view->row_index, $id);
    $empty = $field_output !== 0 && empty($field_output);
    
    
    if($id == $options['start_date']) {
      if (empty($field->options['exclude']) && (!$empty || (empty($field->options['hide_empty']) && empty($vars['options']['hide_empty'])))) {
        $object = new stdClass();
        $start_date = $vars['row']->{$view->field[$id]->field_alias};
        /*
        $content = "
          <div class='col-md-3 col-sm-4 col-xs-4 wrap-event'>
            <div class='block-day'>
              <span class='day'>
                ". date('d', strtotime($start_date))  ."
              </span>
              <span class='month'>
                " . date('F', strtotime($start_date)) . "
              </span>
            </div>
          </div>";

        $object->content = $content;
        if (isset($view->field[$id]->field_alias) && isset($vars['row']->{$view->field[$id]->field_alias})) {
          $object->raw = $vars['row']->{$view->field[$id]->field_alias};
        }
        else {
          $object->raw = NULL; // make sure it exists to reduce NOTICE
        }
        $object->inline = !empty($vars['options']['inline'][$id]);
        $object->inline_html = $object->inline ? 'span' : 'div';
        if (!empty($vars['options']['separator']) && $inline && $object->inline && $object->content) {
          $object->separator = filter_xss_admin($vars['options']['separator']);
        }

        $inline = $object->inline;

        $object->handler = &$view->field[$id];
        $object->element_type = $object->handler->element_type();
        $object->class = views_css_safe($id);
        $object->label = check_plain($view->field[$id]->label());
        $vars['fields'][$id] = $object;
        */
      }
    } else if($id == $options['end_date']) {
      if (empty($field->options['exclude']) && (!$empty || (empty($field->options['hide_empty']) && empty($vars['options']['hide_empty'])))) {
        $end_date = $vars['row']->{$view->field[$id]->field_alias};
      }
    } else if($id == $options['title']) {
      if (empty($field->options['exclude']) && (!$empty || (empty($field->options['hide_empty']) && empty($vars['options']['hide_empty'])))) {
        $title = $vars['row']->{$view->field[$id]->field_alias};
      }
    } else if($id == $options['event_status']) {
      if (empty($field->options['exclude']) && (!$empty || (empty($field->options['hide_empty']) && empty($vars['options']['hide_empty'])))) {
        $event_status = $vars['row']->{$view->field[$id]->field_alias};
      }
    } else {

      $object = new stdClass();

      $startMonth = date('m', strtotime($start_date));
      $endMonth = date('m', strtotime($end_date));
      $startDate = date('d', strtotime($start_date));
      $endDate = date('d', strtotime($end_date));
      $timeOutput = '';

      if($endMonth - $startMonth == 2 || $startDate == 1 && $endDate > 28) {

        $timeOutput = "
          <span class='month-tbd'>
            To Be Determined
          </span>
        ";

        /*switch($startMonth) {
          case 1:
            $timeOutput = 'Q1';
            break;
          case 4:
            $timeOutput = 'Q2';
            break;
          case 7:
            $timeOutput = 'Q3';
            break;
          case 10:
            $timeOutput = 'Q4';
            break;
        }*/

      } else if($startDate == 1 && $endDate > 28) {
        $timeOutput = "
          <span class='day'>
            ".date('M', strtotime($start_date))."
          </span>
        ";
      } else {
        $timeOutput = "
          <span class='day'>
            ". date('d', strtotime($start_date))  ."
          </span>
          <span class='month'>
            " . date('F', strtotime($start_date)) . "
          </span>
        ";
      }

      $content = "
        <div class='col-md-3 col-sm-4 col-xs-4 wrap-event'>
            <div class='block-day text-center'>
              $timeOutput
            </div>
          </div>
        <div class='col-md-9 col-sm-8 col-xs-8 wrap-event'>
          <div class='block-event'>
            <div class=''>
              <div class='range-date'>
                ".date('d F', strtotime($start_date))." - ".date('d F', strtotime($end_date))."
              </div>
            </div>
            <div class='title'>
              <a href='" . url(drupal_get_path_alias('node/' . $vars['row']->nid), array('absolute' => TRUE)) . "'>
              ".$title."
              </a>
            </div>
            <div class='title'>
            </div>
            <div class='category-link'>
              ".$field_output."
            </div>
          </div>
        </div>
      ";
              //Status: ".$vars['row']->node_data_field_start_date_field_event_status_value."
              //Status: ".$event_status."
      
      $title = $vars['row']->{$view->field[$id]->field_alias};
      $object->content = $content;
      if (isset($view->field[$id]->field_alias) && isset($vars['row']->{$view->field[$id]->field_alias})) {
        $object->raw = $vars['row']->{$view->field[$id]->field_alias};
      }
      else {
        $object->raw = NULL; // make sure it exists to reduce NOTICE
      }
      $object->inline = !empty($vars['options']['inline'][$id]);
      $object->inline_html = $object->inline ? 'span' : 'div';
      if (!empty($vars['options']['separator']) && $inline && $object->inline && $object->content) {
        $object->separator = filter_xss_admin($vars['options']['separator']);
      }

      $inline = $object->inline;

      $object->handler = &$view->field[$id];
      $object->element_type = $object->handler->element_type();
      $object->class = views_css_safe($id);
      $object->label = check_plain($view->field[$id]->label());
    
      $vars['fields'][$id] = $object;
    }
  }
}
