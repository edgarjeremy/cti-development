if(Drupal.jsEnabled) {
  $(document).ready(function() {
    if($('#quicklink-admin-settings').length > 0){
      if($('#edit-type-1').val() == "node") {
        $('#edit-text-out-1-wrapper').hide();
        $('#edit-link-out-1-wrapper').hide();
      } else if($('#edit-type-1').val() == "outside") {
        $('#edit-node-1-wrapper').hide();
        $('add-new-node-1').hide();
      } else {
        $('add-new-node-1').hide();
        $('#edit-node-1-wrapper').hide();
        $('#edit-text-out-1-wrapper').hide();
        $('#edit-link-out-1-wrapper').hide();
      }
      
      if($('#edit-type-2').val() == "node") {
        $('#edit-text-out-2-wrapper').hide();
        $('#edit-link-out-2-wrapper').hide();
      } else if($('#edit-type-1').val() == "outside") {
        $('#edit-node-2-wrapper').hide();
        $('add-new-node-2').hide();
      } else {
        $('add-new-node-2').hide();
        $('#edit-node-2-wrapper').hide();
        $('#edit-text-out-2-wrapper').hide();
        $('#edit-link-out-2-wrapper').hide();
      }
      
      if($('#edit-type-3').val() == "node") {
        $('#edit-text-out-3-wrapper').hide();
        $('#edit-link-out-3-wrapper').hide();
      } else if($('#edit-type-1').val() == "outside") {
        $('#edit-node-3-wrapper').hide();
        $('add-new-node-3').hide();
      } else {
        $('#add-new-node-3').hide();
        $('#edit-node-3-wrapper').hide();
        $('#edit-text-out-3-wrapper').hide();
        $('#edit-link-out-3-wrapper').hide();
      }
      
      if($('#edit-type-4').val() == "node") {
        $('#edit-text-out-4-wrapper').hide();
        $('#edit-link-out-4-wrapper').hide();
      } else if($('#edit-type-1').val() == "outside") {
        $('#edit-node-4-wrapper').hide();
        $('#add-new-node-4').hide();
      } else {
        $('#add-new-node-4').hide();
        $('#edit-node-4-wrapper').hide();
        $('#edit-text-out-4-wrapper').hide();
        $('#edit-link-out-4-wrapper').hide();
      }
      
      $('body').append('<div id="dialog-ajax-form" title="Node List"><div id="dialog-node-selector"></div><div id="dialog-node-list"></div></div>');
      
      callbackDialog = function($editNode, $editNodeId) {
        dialog = $( "#dialog-ajax-form" ).dialog({
          autoOpen: false,
          height: 600,
          width: 800,
          modal: true,
          buttons: {
            "Add Node Link": function() {
              $dialogNodeList = $('#dialog-node-list');
              if($dialogNodeList.length > 0) {
                nodeid = $dialogNodeList.find('input[name=nodeid]:checked').val();
                textName = $dialogNodeList.find('label[for=nodeid-'+ nodeid +']').text();
                
                $editNode.val(textName); // change
                $editNodeId.val(nodeid); // change
                dialog.dialog( "close" );
              }
            },
            Cancel: function() {
              dialog.dialog( "close" );
            }
          }
        });
        
        $promise = $.ajax({
          method: "GET",
          url: "../../../api/v1/nodetype/list",
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        });

        $promise.then(function(data, textStatus){
          if(textStatus == 'success') {
            $content = $('#dialog-node-selector');
            $content.html('');
            $dialogNodeList = $('#dialog-node-list');
            $dialogNodeList.html('');
            
            $select = $('<select name="nodetype"></select>');
            options = '<option value="0" selected="selected">Please Select</option>';
            for (var i = 0; i < data.length; i++) {
              options += '<option value="'+data[i].type+'">'+data[i].name+'</option>';
            }
            
            $select.append(options);
            
            $content.append($select);
            
            $select.change(function() {
              var optionSelected = $(this).find("option:selected");
              
              $promise = $.ajax({
                method: "GET",
                url: "../../../api/v1/node/list/"+ optionSelected.val(),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
              });

              $promise.then(function(data, textStatus){
                if(textStatus == 'success') {
                  $dialogNodeList = $('#dialog-node-list');
                  $dialogNodeList.html('');
                  
                  if(data.length > 0) {
                    $form = $('<form action="#"></form>');
                    
                    for (var i = 0; i < data.length; i++) {
                      $radioButton = $('<input type="radio" name="nodeid" value="'+data[i].nid+'" id="nodeid-'+data[i].nid+'"> <label for="nodeid-'+data[i].nid+'">'+data[i].title+'</label><br>');
                      $form.append($radioButton);
                    }
                    
                    $dialogNodeList.append($form);
                  }
                }
              });
            });
            
            dialog.dialog( "open" );
          }
        });
      };
      
      $('#edit-type-1').change(function() {
        var optionSelected = $(this).find("option:selected");
        
        switch(optionSelected.val()) {
          case 'none':
            $('#edit-node-1-wrapper').hide();
            $('#edit-text-out-1-wrapper').hide();
            $('#edit-link-out-1-wrapper').hide();
            $('#add-new-node-1').hide();
            break;
          case 'node':
            $('#edit-node-1-wrapper').show();
            $('#add-new-node-1').show();
            $('#edit-text-out-1-wrapper').hide();
            $('#edit-link-out-1-wrapper').hide();
            break;
          case 'outside':
            $('#edit-text-out-1-wrapper').show();
            $('#edit-link-out-1-wrapper').show();
            $('#edit-node-1-wrapper').hide();
            $('#add-new-node-1').hide();
            break;
        }
      });
      
      $('#add-new-node-1').click(function(){
        callbackDialog($('#edit-node-1'), $('#edit-node-1-id'));
      });
      
      $('#edit-type-2').change(function() {
        var optionSelected = $(this).find("option:selected");
        
        switch(optionSelected.val()) {
          case 'none':
            $('#edit-node-2-wrapper').hide();
            $('#edit-text-out-2-wrapper').hide();
            $('#edit-link-out-2-wrapper').hide();
            $('#add-new-node-2').hide();
            break;
          case 'node':
            $('#edit-node-2-wrapper').show();
            $('#add-new-node-2').show();
            $('#edit-text-out-2-wrapper').hide();
            $('#edit-link-out-2-wrapper').hide();
            break;
          case 'outside':
            $('#edit-text-out-2-wrapper').show();
            $('#edit-link-out-2-wrapper').show();
            $('#edit-node-2-wrapper').hide();
            $('#add-new-node-2').hide();
            break;
        }
      });
      
      $('#add-new-node-2').click(function(){
        callbackDialog($('#edit-node-2'), $('#edit-node-2-id'));
      });
      
      $('#edit-type-3').change(function() {
        var optionSelected = $(this).find("option:selected");
        
        switch(optionSelected.val()) {
          case 'none':
            $('#edit-node-3-wrapper').hide();
            $('#edit-text-out-3-wrapper').hide();
            $('#edit-link-out-3-wrapper').hide();
            $('#add-new-node-3').hide();
            break;
          case 'node':
            $('#edit-node-3-wrapper').show();
            $('#add-new-node-3').show();
            $('#edit-text-out-3-wrapper').hide();
            $('#edit-link-out-3-wrapper').hide();
            break;
          case 'outside':
            $('#edit-text-out-3-wrapper').show();
            $('#edit-link-out-3-wrapper').show();
            $('#edit-node-3-wrapper').hide();
            $('#add-new-node-3').hide();
            break;
        }
      });
      
      $('#add-new-node-3').click(function(){
        callbackDialog($('#edit-node-3'), $('#edit-node-3-id'));
      });
      
      $('#edit-type-4').change(function() {
        var optionSelected = $(this).find("option:selected");
        
        switch(optionSelected.val()) {
          case 'none':
            $('#edit-node-4-wrapper').hide();
            $('#edit-text-out-4-wrapper').hide();
            $('#edit-link-out-4-wrapper').hide();
            $('#add-new-node-4').hide();
            break;
          case 'node':
            $('#edit-node-4-wrapper').show();
            $('#add-new-node-4').show();
            $('#edit-text-out-4-wrapper').hide();
            $('#edit-link-out-4-wrapper').hide();
            break;
          case 'outside':
            $('#edit-text-out-4-wrapper').show();
            $('#edit-link-out-4-wrapper').show();
            $('#edit-node-4-wrapper').hide();
            $('#add-new-node-4').hide();
            break;
        }
      });
      
      $('#add-new-node-4').click(function(){
        callbackDialog($('#edit-node-4'), $('#edit-node-4-id'));
      });
      
    }
  });
}