<?php
/**
 * @file views-view-list.tpl.php
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<?php 
  global $base_url;
  global $base_path;
  
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '');
  $breadcrumb[] = arg(0);
  $breadcrumb[] = arg(1);

  // Set Breadcrumbs
  $breadcrumbs = drupal_set_breadcrumb($breadcrumb);
?>
<section class='thumbnail-collections'>
  <div class='entry-header'>
    <div class='content'>
      <div class="breadcrumb">
        <ol class="breadcrumb">
          <?php foreach($breadcrumbs as $breadcrumb): ?>
            <li><?php echo $breadcrumb; ?></li>
          <?php endforeach; ?>
        </ol>
      </div>
      <h1 style="padding-bottom: 10px"><?php print 'Photo Gallery: ' . arg(1) ?></h1>
      <?php
      print '<div class="gallery-header-details" style="color:#fff; margin-bottom: 30px">Images of the people and environments where CTI-CFF does its work.</div>';
      ?>
    </div>
  </div>
  <div class='entry-content'>
    <ul class="grid-thumb">
      <?php foreach ($rows as $id => $row): ?>
        <li class="<?php print $classes[$id]; ?> grid-item"><?php print $row; ?></li>
      <?php endforeach; ?>
    </ul>
    <br />
    <br />
    <p>All photos are copyright of the Coral Triangle Initiative on Coral Reefs, Fisheries, and Food Security (CTI-CFF). These photos may not be used for commercial purposes.</p>
    <br />
    <br />
  </div>
</section>

<script type="text/javascript">
  $(document).ready(function(){
    target = $('#form-filter').closest('.view')[0];
    
    $('.pg_link').each(function(){
      $(this).on('click',function(){
        breadcrumb = $('div.breadcrumb').html();
        var yearName = $('#year').attr("name");
        yearValue = $('#year').val();
      
        var titleName = $('#title').attr("name");
        titleValue = $('#title').val();
        
        var page = $(this).attr('data-page');
        
        var post = {
          "args": <?php echo json_encode($query_args); ?>,
          "filters": [{ "name": yearName, "value":yearValue }, {"name": titleName, "value":titleValue}],
          "show_per_page": "<?php echo $items_per_page; ?>",
          "query": "<?php echo trim(preg_replace('/\s\s+/', ' ', $query)); ?>",
          "view_name": "<?php echo $view_name; ?>"
        };
        
        if($('.item-list').length > 0) {
          $('.item-list').remove();
        }
      
        $.post("<?php echo $base_url; ?>/api/v1/collection/list/" + page, post, function(response, textStatus,jqXHR){
          $(this).removeClass('views-throbbing');
          // Scroll to the top of the view. This will allow users
          // to browse newly loaded content after e.g. clicking a pager
          // link.
          var offset = $(target).offset();
          // We can't guarantee that the scrollable object should be
          // the body, as the view could be embedded in something
          // more complex such as a modal popup. Recurse up the DOM
          // and scroll the first element that has a non-zero top.
          var scrollTarget = target;
          while ($(scrollTarget).scrollTop() == 0 && $(scrollTarget).parent()) {
            scrollTarget = $(scrollTarget).parent()
          }
          // Only scroll upward
          if (offset.top - 10 < $(scrollTarget).scrollTop()) {
            $(scrollTarget).animate({scrollTop: (offset.top - 10)}, 500);
          }
          // Call all callbacks.
          if (response.__callbacks) {
            $.each(response.__callbacks, function(i, callback) {
              eval(callback)(target, response);
            });
          }
        
          $('#year').val(yearValue);
          $('#title').val(titleValue);
          $('div.breadcrumb').html(breadcrumb);
          new window.CoralCTI.Frontend({});
        }, 'json');
        
        return false;
      });
    });
    
    
    $('#form-filter').submit(function(){
      var yearName = $('#year').attr("name");
      yearValue = $('#year').val();
      
      var titleName = $('#title').attr("name");
      titleValue = $('#title').val();
      breadcrumb = $('div.breadcrumb').html();
      
      var post = {
        "args": <?php echo json_encode($query_args); ?>,
        "filters": [{ "name": yearName, "value":yearValue }, {"name": titleName, "value":titleValue}],
        "show_per_page": "<?php echo $items_per_page; ?>",
        "query": "<?php echo trim(preg_replace('/\s\s+/', ' ', $query)); ?>",
        "view_name": "<?php echo $view_name; ?>"
      };
      
      if($('.item-list').length > 0) {
        $('.item-list').remove();
      }
      
      $.post("<?php echo $base_url; ?>/api/v1/collection/list/1", post, function(response, textStatus,jqXHR){
        $(this).removeClass('views-throbbing');
        // Scroll to the top of the view. This will allow users
        // to browse newly loaded content after e.g. clicking a pager
        // link.
        var offset = $(target).offset();
        // We can't guarantee that the scrollable object should be
        // the body, as the view could be embedded in something
        // more complex such as a modal popup. Recurse up the DOM
        // and scroll the first element that has a non-zero top.
        var scrollTarget = target;
        while ($(scrollTarget).scrollTop() == 0 && $(scrollTarget).parent()) {
          scrollTarget = $(scrollTarget).parent()
        }
        // Only scroll upward
        if (offset.top - 10 < $(scrollTarget).scrollTop()) {
          $(scrollTarget).animate({scrollTop: (offset.top - 10)}, 500);
        }
        // Call all callbacks.
        if (response.__callbacks) {
          $.each(response.__callbacks, function(i, callback) {
            eval(callback)(target, response);
          });
        }
        
        $('#year').val(yearValue);
        $('#title').val(titleValue);
        $('div.breadcrumb').html(breadcrumb);
        new window.CoralCTI.Frontend({});
      }, 'json');
      
      return false;
    });
  });
</script>






