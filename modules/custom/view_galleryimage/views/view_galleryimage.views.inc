<?php

/**
 * Implementation of hook_views_plugins
 */
function view_galleryimage_views_plugins() {
  $path = drupal_get_path('module', 'view_galleryimage');
  $views_path = drupal_get_path('module', 'views');
  require_once "./$path/theme/theme.inc";
  return array(
    'module' => 'view_galleryimage',
    'style' => array(
      'view_galleryimage_style' => array(
        'title' => t('CTICFF Gallery Images View'),
        'help' => t('Displays nodes.'),
        'handler' => 'view_galleryimage_plugin_style',
        'path' => "$path/views",
        'theme' => 'view_galleryimage_view_style',
        'theme file' => 'theme.inc',
        'theme path' => "$path/theme",
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
    'row' => array(
      'view_galleryimage_row' => array(
        'title' => t('CTICFF Gallery Images Row'),
        'help' => t('(Displays a CTICFF Format Row'),
        'handler' => 'view_galleryimage_plugin_row',
        'path' => "$path/views",
        'theme' => 'view_galleryimage_view_row',
        'theme file' => 'theme.inc',
        'theme path' => "$path/theme",
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
    )
  );
}
